#include <ros/ros.h>
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include <vector>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
#include <tf/transform_datatypes.h>
#include <cmath>
ros::Publisher pub, pub_chemin, pub_pt;
std::vector<geometry_msgs::PointStamped> chemin;
int taille = 100000;
float K = 1;
float l1 = 0.7;
bool fin;
nav_msgs::Path chemin_rviz;
std::vector<geometry_msgs::PointStamped> create_chemin(){
	std::vector<geometry_msgs::PointStamped> c;
	for(int i = 0 ; i < taille ; i++){
		geometry_msgs::PointStamped p;
		geometry_msgs::PoseStamped pose;
		p.point.z = 0;
		p.point.y = 0.01;
		p.point.x = 0.01*i;
		p.header.frame_id = "/odom";
		p.header.stamp = ros::Time::now();
		pose.pose.position = p.point;
		pose.header.frame_id = "/odom";
		pose.header.stamp = ros::Time::now();
		chemin_rviz.poses.push_back(pose);
		c.push_back(p);
	}
	chemin_rviz.header.frame_id = "/odom";
	chemin_rviz.header.stamp = ros::Time::now();
	return c;
}

void callback2(nav_msgs::Path p){
	chemin.clear();
	chemin_rviz.poses.clear();
	fin = false;
	for(int i = 0 ; i < p.poses.size(); i++){
		geometry_msgs::PointStamped point_st;
// 		point_st.header.frame_id = "/odom";
		point_st.header.frame_id = p.header.frame_id;
		point_st.point.x = p.poses[i].pose.position.x;
		point_st.point.y = p.poses[i].pose.position.y;
		point_st.point.z = p.poses[i].pose.position.z;
		
		chemin_rviz.poses.push_back(p.poses[i]);
		chemin.push_back(point_st);
	}
// 	chemin_rviz.header.frame_id = "/odom";
	chemin_rviz.header.frame_id = p.header.frame_id;
	chemin_rviz.header.stamp = ros::Time::now();
	
	pub_chemin.publish(chemin_rviz);
}

float dist(geometry_msgs::PointStamped p1, float x, float y ){
	return sqrt(pow(p1.point.x - x,2) + pow(p1.point.y - y,2));/* + pow(p1.z - p2.position.z,2))*/
}

geometry_msgs::PointStamped orth(geometry_msgs::PointStamped p){
	geometry_msgs::PointStamped ret;
	ret.point.x = 1/(p.point.y + (pow(p.point.x,2)/p.point.y));
	ret.point.y = -(p.point.x/p.point.y)/(p.point.y + (pow(p.point.x,2)/p.point.y));
	return ret;
}

void callback(nav_msgs::Odometry odom){
	
	geometry_msgs::Twist t;
	if(!fin){
		if(chemin.size() == 0)
			return;
		pub_chemin.publish(chemin_rviz);
		float x = odom.pose.pose.position.x, y = odom.pose.pose.position.y, theta = 0;
		float u1 = 0.3, distance = -1.0, x_chemin = 0, y_chemin = 0;
		int temp_i= 0;
		theta = tf::getYaw(odom.pose.pose.orientation);
		
		for(int i = 0 ; i < chemin.size() ; i ++){
			float temp = dist(chemin[i],x,y);
			if(distance == -1 || distance > temp){
				temp_i = i;
				distance = temp;
				x_chemin = chemin[i].point.x;
				y_chemin = chemin[i].point.y;
			}
		}
// 		chemin[temp_i].header.frame_id = "/";
		pub_pt.publish(chemin[temp_i]);
		if(distance < 0.35 && chemin.size() - temp_i < chemin.size() * 0.01){	//Si on est proche de la fin on efface le chemin
			chemin.clear();
			ROS_INFO("ERASED");
		}
		geometry_msgs::PointStamped Ys;
		Ys.point.x = x_chemin - x;
		Ys.point.y = y_chemin - y;
		
		geometry_msgs::PointStamped Xs;
		Xs = orth(Ys);
		float theta_chemin = atan2(-Xs.point.y,-Xs.point.x);
		float theta_error = -theta_chemin + theta ;
		float u2 = 0;
		
	// 	ROS_INFO("%f",theta_error);	
		K = exp(distance);	
		u2 = (-u1 * sin(theta_error)/(l1*cos(theta_error)) - K*u1 * distance/cos(theta_error));
		if(u2 > 0.8)
			u2 = 0.8;
		else if(u2 < -0.8)
			u2 = -0.8;
		t.angular.x = 0.0;
		t.angular.y = 0.0;
		t.angular.z = u2;
		t.linear.x = u1;
		t.linear.y = 0.0;
		t.linear.z = 0.0;
		
	}
	else{
		t.angular.x = 0.0;
		t.angular.y = 0.0;
		t.angular.z = 0.0;
		t.linear.x = 0.0;
		t.linear.y = 0.0;
		t.linear.z = 0.0;
	}
	
	pub.publish(t);
}


int main(int argc, char **argv){	
	ros::init (argc, argv, "commande");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("/odom",1,callback);
	ros::Subscriber sub2 = n.subscribe("/Waypoints_robot",1,callback2);
	pub = n.advertise<geometry_msgs::Twist>("/cmd_vel",1);
	pub_chemin = n.advertise<nav_msgs::Path>("/chemin",1);
	pub_pt = n.advertise<geometry_msgs::PointStamped>("/pt",1);
	
// 	chemin = create_chemin();
	ros::spin();
	return 0;
}
	