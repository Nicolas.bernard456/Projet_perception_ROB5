#include "ros/ros.h"
#include <commande/Twist_ordre.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/PointCloud.h>
// On controle la vitesse du robot
ros::Publisher move_publisher;
ros::Subscriber sensor_subscriber,ir_sub1,ir_sub2,ir_sub3,ir_sub4,ir_sub5;
commande::Twist_ordre mouvement;// Vitesse robot
ros::Time t;


bool decoincement = false;
int compteur_detection = 0;
std::vector<float> ranges;

void initMove( commande::Twist_ordre& m){
    m.t.angular.x = 0;
    m.t.angular.y = 0;
    m.t.angular.z = 0;
    m.t.linear.x = 0.5;
    m.t.linear.y = 0;
    m.t.linear.z = 0;
    
  
}

void sensor_callback(){

	int g1 = 0, g2 = 0, d1 = 0 , d2 = 0;
	initMove(mouvement);
	for(int i = 0 ; i < ranges.size() ; i ++){
		std::cout << ranges[i] << std::endl;
		if(ranges[i] < 0.55){
			if(i == 0) 
				d1++;
			else if( i <= 2) 
				d2++;
			else if(i <= 4) 
				g1++;
			else
				g2++;
		}
	}
	mouvement.t.linear.x = 0.3;
	mouvement.t.angular.z = 0.0;
	std::cout << g1 << " " << g2 << " " << d1 << " " << d2 << std::endl;
	std::cout << (g1 + g2) - (d1 + d2) << std::endl;

	if((g1 + g2) > (d1 + d2) && (g1 + g2) - (d1 + d2) != 0){
		mouvement.t.linear.x = 0;
		mouvement.t.angular.z = 1.0;
	}
	else{
		if((g1 + g2) < (d1 + d2)){
			mouvement.t.linear.x = 0;
			mouvement.t.angular.z = -1.0;
			compteur_detection++;
		}
	}
	if(mouvement.t.linear.x == 0)
		mouvement.active = true;
	else
		mouvement.active = false;
	move_publisher.publish(mouvement); // On publie la vitesse du robot

}

void sub1_callback(sensor_msgs::PointCloud r){

	ranges[0] = r.points[1].x;
	ranges[1] = r.points[2].x;
	ranges[2] = r.points[3].x;
	ranges[3] = r.points[4].x;
	ranges[4] = r.points[5].x;
	ranges[5] = r.points[6].x;
}



int main(int argc, char** argv){
	ros::init(argc, argv, "evitement_obstacle");
	ros::NodeHandle n; // declaration noeud
	move_publisher = n.advertise<commande::Twist_ordre>("/cmd_vel_ordre_2",10);// creation d'un publisher pour le topic cmd_vel
	for(int i = 0 ; i < 6 ; i++)
		ranges.push_back(0.0);
	ir_sub1 = n.subscribe<sensor_msgs::PointCloud>("/RosAria/sonar",1, sub1_callback);	//sensor_subscriber = n.subscribe<sensor_msgs::LaserScan>("/camera/depth/points",1, sensor_callback);// bumper_state
	//sensor_subscriber = n.subscribe<sensor_msgs::LaserScan>("/camera/depth/points",1, sensor_callback);// bumper_state
	ros::Rate loop(100.0);
	while(ros::ok()){
		sensor_callback();
		loop.sleep();
		ros::spinOnce();
	}
// 	ros::spin();
}