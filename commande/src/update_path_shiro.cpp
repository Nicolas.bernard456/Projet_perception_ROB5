#include "ros/ros.h"
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Bool.h>
#include <nav_msgs/OccupancyGrid.h>
#include <commande/Twist_ordre.h>
using namespace std;
ros::Publisher publisher, publisher_wait_commande;
ros::Subscriber subs, sub_map, sub_stop;
bool path_recu;
nav_msgs::Path chemin_update, chemin;
nav_msgs::OccupancyGrid origin_map, new_map;
bool origin_computed, chemin_recu;
std_msgs::Bool wait_cmd;

nav_msgs::Path change_frame_path(nav_msgs::Path path, nav_msgs::OccupancyGrid map_origin, nav_msgs::OccupancyGrid map_new);
//void EnvoieChemin( ros::NodeHandle n, nav_msgs::Path chemin);
void Chemin_callback(nav_msgs::Path data);
//void subscriber_chemin(ros::NodeHandle n);
void map_callback(nav_msgs::OccupancyGrid data);
void stop_callback(commande::Twist_ordre data);
// update the path when the frame changes
int main(int argc, char** argv){
	wait_cmd.data = false;
	ros::init(argc, argv, "update_path_shiro");
	ros::NodeHandle n; // declaration noeud
	origin_computed = false;
	chemin_recu = false;
	// On recupere le chemin
	publisher = n.advertise<nav_msgs::Path>("Waypoints_robot",1);
	publisher_wait_commande = n.advertise<std_msgs::Bool>("wait_commande",1); 
	subs = n.subscribe("Chemin_t",1, Chemin_callback);
	sub_map = n.subscribe("map", 1, map_callback);
	sub_stop = n.subscribe("cmd_vel_ordre_1", 1, stop_callback);
	ros::spin();
	
	
	/*subscriber_chemin(n);
	 * subscriber_map_origin(n);
	 * // On le met à jour tous le temps
	 * ros::Rate r(10);
	 * while(ros::ok()){
	 *   chemin_update.header.stamp = ros::Time::now();
	 *   EnvoieChemin(n, chemin_update);
	 *   
	 *   ros::spinOnce();
	 *   r.sleep();
	 *   
	 *   
}*/
	
}

/*void EnvoieChemin( ros::NodeHandle n, nav_msgs::Path chemin){
 * publisher = n.advertise<nav_msgs::Path>("Waypoints_robot",100);//declaration publisher vers 
 * ros::Rate r(1000);
 * 
 * while(publisher.getNumSubscribers() == 0){
 *     
 *     r.sleep();
 *  
 * }
 * publisher.publish(chemin);
 * ros::spinOnce();
 * publisher.publish(chemin);
 *   cout << " Chemin recu par la commande "<< endl;
 } *   */

// Receive the path in the topic "Chemin_t"
void Chemin_callback(nav_msgs::Path data){
	
	chemin = data;
	chemin_recu = true;
	origin_computed = false;
	std::cout << "chemin recu " << std::endl;
	publisher.publish(chemin);
	sleep(0.1);
	publisher_wait_commande.publish(wait_cmd);
}

// Receive the map when their are changement
void map_callback(nav_msgs::OccupancyGrid data){
	if(chemin_recu == true){
		if(origin_computed == false){
			origin_map = data;
			chemin_update = change_frame_path(chemin, origin_map, origin_map);
			origin_computed = true;
		}
		else{
			new_map = data; 
			chemin_update = change_frame_path(chemin, origin_map, new_map);
		}
		std::cout << "envoie commande " << std::endl;
		publisher.publish(chemin_update);
	}  
}

void stop_callback(commande::Twist_ordre data){
	if(data.active == false){
		chemin_recu = false;
		origin_computed= false;
		chemin_update.poses.clear();
		chemin.poses.clear();
	}
}

/*void subscriber_chemin(ros::NodeHandle n){
 * path_recu= false;
 * subs = n.subscribe("Chemin_t",1, &Chemin_callback);
 * ros::Rate r(100);
 * //cout << "avant while " << BoolMap << endl;
 * while(n.ok() &&  path_recu == false){
 *   
 *   ros::spinOnce();
 *   
 * }
 * subs.shutdown();
 * 
 } *
 
 // subscriber of the origin map 
 void subscriber_map_origin(ros::NodeHandle n){
 sub_map = n.subscribe("map",1, &Chemin_callback);
 ros::Rate r(100);
 //cout << "avant while " << BoolMap << endl;
 while(n.ok() &&  path_recu == false){
	 
	 ros::spinOnce();
	 
	 }
	 subs.shutdown();
	 
	 }*/

// Convert when the map in the referentiel map change because of gmapping
nav_msgs::Path change_frame_path(nav_msgs::Path path, nav_msgs::OccupancyGrid map_origin, nav_msgs::OccupancyGrid map_new){
	nav_msgs::Path path_dest;
	path_dest.header.frame_id = new_map.header.frame_id;
	path_dest.header.stamp = new_map.header.stamp;
	for(int i = 0 ; i < path.poses.size() ; i++){
		geometry_msgs::PoseStamped pose;
		pose.pose.position.x = path.poses[i].pose.position.x - map_origin.info.origin.position.x + map_new.info.origin.position.x;
		pose.pose.position.y = path.poses[i].pose.position.y - map_origin.info.origin.position.y + map_new.info.origin.position.y;
		path_dest.poses.push_back(pose);
	}
	return path_dest;

}