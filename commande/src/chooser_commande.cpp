#include <ros/ros.h>
#include <commande/Twist_ordre.h>
#include <vector>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PointStamped.h>
std::vector<commande::Twist_ordre> tab_cmd;
std::vector<ros::Time> tab_cmd_time;
bool waiter_cmd;
void cb0(commande::Twist_ordre ordre0){
	tab_cmd[0] = ordre0;
	tab_cmd_time[0] = ros::Time::now();
}

void cb1(commande::Twist_ordre ordre1){
	tab_cmd[1] = ordre1;
	tab_cmd_time[1] = ros::Time::now();
}

void cb2(commande::Twist_ordre ordre2){
	tab_cmd[2] = ordre2;
	tab_cmd_time[2] = ros::Time::now();
}

void cb_wait_cmd(std_msgs::Bool waiter){
	waiter_cmd = waiter.data;
}

void cb_wait_cmd2(geometry_msgs::PointStamped ps){
	waiter_cmd = false;
}




int main(int argc, char **argv){
	ros::init(argc,argv,"chooser_commande");
	ros::NodeHandle n;
	commande::Twist_ordre final_order, null_order;
	null_order.t.angular.x = 0.0;
	null_order.t.angular.y = 0.0;
	null_order.t.angular.z = 0.0;
	null_order.t.linear.x = 0.0;
	null_order.t.linear.y = 0.0;
	null_order.t.linear.z = 0.0;
	for(int i = 0 ; i < 3 ; i++){
		tab_cmd.push_back(null_order);
		tab_cmd_time.push_back(ros::Time::now());
	}
	ros::Subscriber sub_wait_commande = n.subscribe("/wait_commande",1, cb_wait_cmd);
	ros::Subscriber sub_mapping_alea = n.subscribe("cmd_vel_ordre_0", 1 , cb0);
	ros::Subscriber sub_commande = n.subscribe("cmd_vel_ordre_1", 1 , cb1);
	ros::Subscriber sub_evitement = n.subscribe("cmd_vel_ordre_2", 1 , cb2);
	ros::Publisher pub = n.advertise<geometry_msgs::Twist>("cmd_vel",1);
	ros::Subscriber sub2 = n.subscribe("/clicked_point",1 ,cb_wait_cmd2);
	
	
	ros::Rate loop(100);
	while(ros::ok()){
		if(!waiter_cmd){
			final_order = null_order;
			for(int i = 0 ; i < tab_cmd.size() ; i ++){
				if(tab_cmd[i].active)
					final_order = tab_cmd[i];
				if(ros::Time::now() - tab_cmd_time[i] > ros::Duration(1.0))
					tab_cmd[i] = null_order;
			}
		}
		else 
			final_order = null_order;
		pub.publish(final_order.t);
		loop.sleep();
		ros::spinOnce();
	}
	
	
	return 0;
}