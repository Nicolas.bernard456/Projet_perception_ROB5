#include "Noeud.hpp"

#include <stdio.h>

Noeud::Noeud()
{

}

Noeud::~Noeud()
{

}

bool check_obstacle(cv::LineIterator l, cv::Mat *image){
	
	cv::Mat img_copy = image->clone();
	cv::cvtColor( *image, img_copy, CV_BGR2GRAY );
	for(int i = 0 ; i < l.count ; i++,l++){
		if((int)img_copy.at<uchar>(l.pos()) != 254){
			return false;
		}
	}
	return true;
}


void Noeud::init_noeud(int numero_noeud_, std::vector< int > data_, int size_pix_){
	numero_noeud = numero_noeud_;
	for(int i = 0; i < data_.size() ; i ++)
		data.push_back(data_[i]);
	size_pix = size_pix_;
}

void Noeud::init_noeud_es(int numero_noeud_, int x_centre_, int y_centre_, int size_pix_){
	numero_noeud = numero_noeud_;
	x_centre = x_centre_;
	y_centre = y_centre_;
	size_pix = size_pix_;
}



void Noeud::ini_centre(int x_centre_, int y_centre_){
	x_centre = x_centre_ - size_pix/2;
	y_centre = y_centre_ - size_pix/2;
}

int Noeud::get_num_noeud(){
	return numero_noeud;
}

void Noeud::create_output(cv::Mat *image){

	if(! image->data ){
		std::cout <<  "Could not open or find the image" << std::endl ;
		return ;
	}

	cv::Point Pt1, Pt2;
	Pt1.x = x_centre;	//Centre du cercle (x)
	Pt1.y = image->rows - y_centre;	//Centre du point (y) Les reperes sont differents (BAS =0 sur ROS et HAUT =0 sur openCv)
	cv::circle(*image,Pt1,size_pix/5,cv::Scalar(0,0,255), -1 );
}


void Noeud::color_chemin(cv::Mat* image_){
	if(! image_->data ){
		std::cout <<  "Could not open or find the image" << std::endl ;
		return ;
	}
	cv::Point Pt1, Pt2;
	Pt1.x = x_centre;	//Centre du cercle (x)
	Pt1.y = image_->rows - y_centre;	//Centre du point (y) Les reperes sont differents (BAS =0 sur ROS et HAUT =0 sur openCv)
	cv::circle(*image_,Pt1,size_pix/5,cv::Scalar(255,0,0), -1 );
	Pt2.x = get_antecedent()->getX();
	Pt2.y = image_->rows - get_antecedent()->getY();
	cv::line(*image_, Pt1, Pt2,cv::Scalar(0,0,255),5);
}


void Noeud::display(cv::Mat *image){
	cv::namedWindow( "Display", cv::WINDOW_NORMAL );// Create a window for display.
	
	if (!image->empty()) {
		cv::imshow( "Display", *image );                   // Show our image inside it.
	}
	cv::waitKey(0);                                          // Wait for a keystroke in the window
}


int Noeud::getX(){
	return x_centre;
}


int Noeud::getY(){
	return y_centre;
}



bool Noeud::check_adjac(Noeud* Noeud_adj, cv::Mat* image){
	if(abs(Noeud_adj->getX() - x_centre) == size_pix && Noeud_adj->getY() == y_centre){
		cv::LineIterator lI(*image,cv::Point(x_centre,image->rows - y_centre), cv::Point(Noeud_adj->getX(),image->rows - Noeud_adj->getY()));
		if(check_obstacle(lI,image)){
			adjac.push_back(Noeud_adj);
			return true;
		}
	}
	if(abs(Noeud_adj->getY() - y_centre) == size_pix && Noeud_adj->getX() == x_centre){
		cv::LineIterator lI(*image,cv::Point(x_centre,image->rows - y_centre), cv::Point(Noeud_adj->getX(),image->rows - Noeud_adj->getY()));
		if(check_obstacle(lI,image)){
			adjac.push_back(Noeud_adj);
			return true;
		}
	}
	return false;
}

bool Noeud::check_adjac_es(Noeud* Noeud_adj, cv::Mat* image){
	if(abs(Noeud_adj->getX() - x_centre) <= size_pix && abs(Noeud_adj->getY() - y_centre) <= size_pix){
		cv::LineIterator lI(*image,cv::Point(x_centre,image->rows - y_centre), cv::Point(Noeud_adj->getX(),image->rows - Noeud_adj->getY()));
// 		if(check_obstacle(lI,image)){
			adjac.push_back(Noeud_adj);
			return true;
// 		}
	}
	return false;
}

bool Noeud::check_adjac_es2(Noeud* Noeud_adj, cv::Mat* image){
	if(abs(Noeud_adj->getX() - x_centre) <= size_pix && abs(Noeud_adj->getY() - y_centre) <= size_pix){
		cv::LineIterator lI(*image,cv::Point(x_centre,image->rows - y_centre), cv::Point(Noeud_adj->getX(),image->rows - Noeud_adj->getY()));
		if(check_obstacle(lI,image)){
			adjac.push_back(Noeud_adj);
			return true;
		}
	}
	return false;
}


std::vector< Noeud* > Noeud::get_adjac(){
	return adjac;
}

Noeud* Noeud::get_antecedent(){
	return antecedent;
}

void Noeud::set_antecedent(Noeud* antecedent_){
	antecedent = antecedent_;
}


bool Noeud::antecedent_exist(){
	if(antecedent)
		return true;
	else
		return false;
}

void Noeud::clear_antec(){
	antecedent = NULL;
}


void Noeud::clear_adjac(){
	adjac.clear();
}
