#ifndef GRAPH_HPP
#define GRAPH_HPP
#include <ros/ros.h>
#include "Noeud.hpp"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PointStamped.h"
#include "nav_msgs/Path.h"
class Graph{
	private : 
		std::vector<Noeud> graph;
		std::vector<int> poid;
		std::vector<bool> visit;
		
		
		
	public:
		Graph();
		~Graph();
		void reset();
		void Add_noeud(const Noeud& n);
		const int get_size();
		Noeud getNoeud(int indice);
		bool init_connexion(cv::Mat *image);
		bool init_connexion2(cv::Mat* image, int taille_max);
// 		void display();
		nav_msgs::Path dijkstra(bool display, nav_msgs::OccupancyGrid map, int id_depart, int id_arrivee, cv::Mat *image);
		nav_msgs::Path Astar(bool display, nav_msgs::OccupancyGrid map, int id_depart, int id_arrivee, cv::Mat* image);
// 		int min_poid();
		int recherche_noeud_min();
		bool condition_noeud_fils(Noeud* fils, Noeud* pere);
		float dist_pose(geometry_msgs::PoseStamped p1 , geometry_msgs::PoseStamped p2);
		void delete_last_noeud(int* cpt);
		void remove_line(int id_arrivee);
		bool condition_noeud_fils_dikjstra(Noeud* fils, Noeud* pere);
		bool condition_noeud_fils_Astar(Noeud* fils, Noeud* pere, int id_arrivee);
		std::vector <geometry_msgs::PoseStamped> add_point(std::vector <geometry_msgs::PoseStamped> p);
};



#endif