#include "graph.hpp"


inline float distance_eucli(Noeud* n1, Noeud* n2){
	return sqrt(pow(n2->getX() - n1->getX(),2 ) + pow(n2->getY() - n1->getY(),2 ));
}

Graph::Graph(){

}


Graph::~Graph()
{

}

void Graph::reset(){
	graph.clear();
	poid.clear();
	visit.clear();
}


void Graph::Add_noeud(const Noeud& n){
	graph.push_back(n);
}

const int Graph::get_size(){
	return graph.size();
}

Noeud Graph::getNoeud(int indice){
	if(indice < graph.size())
		return graph[indice];
	else{
		std::cout << "Erreur indice hors du tableau" << std::endl;
		return Noeud();
	}
}

bool Graph::init_connexion(cv::Mat* image){	//Init les connexions pour les deux dernieres positions entrees
	bool res = false;
	for(int i = 0 ; i < get_size() ; i++){
		graph[i].clear_adjac();
		for(int j = 0; j < get_size() ; j++){
			if(graph[i].check_adjac_es(&(graph[j]), image))
				res = true;
		}
	}
	return res;
}

bool Graph::init_connexion2(cv::Mat* image, int taille_max){	//Init les connexions pour les deux dernieres positions entrees
	bool res = false;
	for(int i = 0 ; i < get_size() ; i++){
		graph[i].clear_adjac();
		if(i < taille_max){
			for(int j = 0; j < get_size() ; j++){
				if(graph[i].check_adjac_es(&(graph[j]), image))
					res = true;
			}
		}
		else{
			for(int j = 0; j < get_size() ; j++){
				if(graph[i].check_adjac_es2(&(graph[j]), image))
					res = true;
			}
		}
	}
	return res;
}





int Graph::recherche_noeud_min(){	//Recherche le poid min dans toutes les cases non visité
	int min_poid = -1, indice = -1;
	for(int i = 0 ; i < get_size() ; i++){
		if(visit[i] == false && (min_poid == -1 || (min_poid > poid[i]) && poid[i] != -1)){
			min_poid = poid[i];
			indice = i;
		}
	}
	visit[indice] = true;
	return indice;
}

bool Graph::condition_noeud_fils_dikjstra( Noeud* fils, Noeud* pere){
	if( (!visit[fils->get_num_noeud()]))
		if( (poid[pere->get_num_noeud()] + distance_eucli(pere,fils) < poid[fils->get_num_noeud()]) || poid[fils->get_num_noeud()] == -1)
			return true;
	return false;
	
}


void Graph::delete_last_noeud(int* cpt){
	if(graph.size() >= 2){
		std::cout << graph[graph.size() - 1].get_num_noeud() << std::endl;
		graph.pop_back();
		std::cout << graph[graph.size() - 1].get_num_noeud() << std::endl;
		graph.pop_back();
		std::cout << graph[graph.size() - 1].get_num_noeud() << std::endl;
		*cpt = *cpt - 2;
	}
	else
		ROS_ERROR("Trying to delete empty vector");
}



nav_msgs::Path Graph::dijkstra(bool display, nav_msgs::OccupancyGrid map, int id_depart, int id_arrivee, cv::Mat *image){
	for(int i = 0 ; i < get_size(); i++){
		graph[i].create_output(image);
		if(poid.size() == get_size()){
			poid[i] = -1;
			visit[i] = false;
		}
		else{
			poid.push_back(-1);
			visit.push_back(false);
		}
		graph[i].clear_antec();
	}
	//On place le poid du Noeud 0 à 0 pour dire qu'on commence ici (premiers tests) Utiliser les arguments de la fonction
	poid[id_depart] = 0;
	Noeud* pere = &(graph[id_depart]);
	while(pere->get_num_noeud() != id_arrivee){
		std::vector<Noeud*> fils = pere->get_adjac();
		for(int i = 0 ; i < fils.size() ; i++){
			if(condition_noeud_fils_dikjstra(fils[i],pere)){
				poid[fils[i]->get_num_noeud()] = poid[pere->get_num_noeud()] + distance_eucli(pere,fils[i]);
				fils[i]->set_antecedent(pere);
			}
		}
		pere = &(graph[recherche_noeud_min()]);
	}
	nav_msgs::Path path;
	geometry_msgs::PoseStamped pose;
	path.header.frame_id = "/map";
	std::vector <geometry_msgs::PoseStamped> pose_inverse,pose_inverse2;
	Noeud* antecedent = graph[id_arrivee].get_antecedent();
	if(!antecedent)
		return path;
	if(display)
		graph[id_arrivee].color_chemin(image);
// 	pose.pose.position.x = graph[id_arrivee].getX() * map.info.resolution + map.info.origin.position.x;
// 	pose.pose.position.y = graph[id_arrivee].getY() * map.info.resolution + map.info.origin.position.y;
	pose.pose.position.x = graph[id_arrivee].getX();
	pose.pose.position.y = graph[id_arrivee].getY();
	pose_inverse.push_back(pose);
	
	while( antecedent->antecedent_exist()){
		if(!antecedent)
			return path;
		antecedent = antecedent->get_antecedent();
	}
	antecedent = graph[id_arrivee].get_antecedent();
	
	
// 	std::cout << "Simplification du chemins ( on retire les points inutiles)" << std::endl;
// 	remove_line(id_arrivee);
// 	std::cout << "Fin simplification du chemins ( on retire les points inutiles)" << std::endl;
	
	while(antecedent->antecedent_exist()){
		if(!antecedent)
			return path;
		if(display){
			antecedent->color_chemin(image);
		}
// 		pose.pose.position.x = antecedent->getX() * map.info.resolution + map.info.origin.position.x;
// 		pose.pose.position.y = antecedent->getY() * map.info.resolution + map.info.origin.position.y;
		pose.pose.position.x = antecedent->getX();
		pose.pose.position.y = antecedent->getY();
		pose_inverse.push_back(pose);
		antecedent = antecedent->get_antecedent();
		if(!antecedent)
			return path;
	}
	std::cout << pose_inverse.size() << std::endl;
	pose_inverse2 = add_point(pose_inverse);
	std::cout << pose_inverse2.size() << std::endl;
	
	for(int i = 0 ; i < pose_inverse2.size() ; i++){
		path.poses.push_back(pose_inverse2[pose_inverse2.size() - i - 1]);
	}
// 	pub.publish(path);
	return path;
}


bool Graph::condition_noeud_fils_Astar(Noeud* fils, Noeud* pere, int id_arrivee){
	if( (!visit[fils->get_num_noeud()]))
		if( (poid[pere->get_num_noeud()] + distance_eucli(&(graph[id_arrivee]),fils) + distance_eucli(pere,fils) < poid[fils->get_num_noeud()]) || poid[fils->get_num_noeud()] == -1)
			return true;
	return false;
	
}



nav_msgs::Path Graph::Astar(bool display, nav_msgs::OccupancyGrid map, int id_depart, int id_arrivee , cv::Mat *image){
	for(int i = 0 ; i < get_size(); i++){
		
		graph[i].create_output(image);
		if(poid.size() == get_size() && visit.size() == get_size()){
			poid[i] = -1;
			visit[i] = false;
		}
		else{
			if(poid.size() != get_size())
				poid.push_back(-1);
			if(visit.size() != get_size())
				visit.push_back(false);
		}
		graph[i].clear_antec();
	}	
	poid[id_depart] = 0;
	Noeud* pere;
	while(pere->get_num_noeud() != id_arrivee){					//Tant que le noeud courrant n'est pas le noeud d arrivee
		std::vector<Noeud*> fils = pere->get_adjac();				//On recupere les noeuds adjacents
		for(int i = 0 ; i < fils.size() ; i++){					//Parcours des fils
			if(condition_noeud_fils_Astar(fils[i],pere,id_arrivee)){	
				poid[fils[i]->get_num_noeud()] = poid[pere->get_num_noeud()] + distance_eucli(&(graph[id_arrivee]),fils[i]) + distance_eucli(pere,fils[i]);
				fils[i]->set_antecedent(pere);
			}
		}
		
		pere = &(graph[recherche_noeud_min()]);
	}
	nav_msgs::Path path;
	geometry_msgs::PoseStamped pose;
	path.header.frame_id = "/map";
	std::vector <geometry_msgs::PoseStamped> pose_inverse,pose_inverse2;
	Noeud* antecedent = graph[id_arrivee].get_antecedent();
	if(!antecedent)
		return path;
	if(display)
		graph[id_arrivee].color_chemin(image);
// 	pose.pose.position.x = graph[id_arrivee].getX() * map.info.resolution + map.info.origin.position.x;
// 	pose.pose.position.y = graph[id_arrivee].getY() * map.info.resolution + map.info.origin.position.y;
	pose.pose.position.x = graph[id_arrivee].getX();
	pose.pose.position.y = graph[id_arrivee].getY();
	pose_inverse.push_back(pose);
	while( antecedent->antecedent_exist()){
		if(!antecedent)
			return path;
		antecedent = antecedent->get_antecedent();
	}
	antecedent = graph[id_arrivee].get_antecedent();
	
// 	std::cout << "Simplification du chemins ( on retire les points inutiles)" << std::endl;
// 	remove_line(id_arrivee);
// 	std::cout << "Fin simplification du chemins ( on retire les points inutiles)" << std::endl;
	
	
	while( antecedent->antecedent_exist()){
		if(!antecedent)
			return path;
		if(display)
			antecedent->color_chemin(image);
// 		pose.pose.position.x = antecedent->getX() * map.info.resolution + map.info.origin.position.x;
// 		pose.pose.position.y = antecedent->getY() * map.info.resolution + map.info.origin.position.y;
		pose.pose.position.x = antecedent->getX();
		pose.pose.position.y = antecedent->getY();
		pose_inverse.push_back(pose);
		antecedent = antecedent->get_antecedent();
		if(!antecedent)
			return path;
	}
	std::cout << pose_inverse.size() << std::endl;
	pose_inverse2 = add_point(pose_inverse);
	std::cout << pose_inverse2.size() << std::endl;
	for(int i = 0 ; i < pose_inverse2.size() ; i++){
		path.poses.push_back(pose_inverse2[pose_inverse2.size() - i - 1]);
	}
// 	pub.publish(path);
	return path;
}

float Graph::dist_pose(geometry_msgs::PoseStamped p1 , geometry_msgs::PoseStamped p2){
	return sqrt(pow((p1.pose.position.x - p2.pose.position.x),2) + pow((p1.pose.position.y - p2.pose.position.y),2));

}


std::vector< geometry_msgs::PoseStamped > Graph::add_point(std::vector< geometry_msgs::PoseStamped > p){
	bool far_away = true;
	std::vector<geometry_msgs::PoseStamped>::iterator it;
	while(far_away){
		far_away = false;
		for(int i = 0 ; i < p.size() - 1 ; i++){
			it = p.begin() + i;
			float distance = dist_pose(p[i],p[i+1]);
			if(distance > 1){
				far_away = true;
				geometry_msgs::PoseStamped interm;
				interm.header.frame_id = p[i].header.frame_id;
				interm.header.stamp = ros::Time::now();
				interm.pose.position.x = (p[i].pose.position.x + p[i+1].pose.position.x) / 2;
				interm.pose.position.y = (p[i].pose.position.y + p[i+1].pose.position.y) / 2;
				p.insert(it + 1,interm);
			}
		}
	}
	std::cout << p.size() << std::endl;
	return p;
}






void Graph::remove_line(int id_arrivee){
	bool cont = true;
	bool horizontal = false, vertical = false;
	double coeff_dir = 0.0, coeff_dir_prec = 0.0;
	int taille = 0, cpt = 0;
	Noeud* antecedent = graph[id_arrivee].get_antecedent();
	Noeud* antecedent2 = graph[id_arrivee].get_antecedent();
	while( antecedent->antecedent_exist() ){
		taille++;
		antecedent = antecedent->get_antecedent();
	}
	while(cont){
		cont = false;
		cpt = 0;
		coeff_dir = 0.0;
		coeff_dir_prec = 0.0;
		antecedent = &(graph[id_arrivee]);
		antecedent2 = &(graph[id_arrivee]);
		while( antecedent->antecedent_exist() && !cont){
			if(antecedent->getY() - antecedent->get_antecedent()->getY() != 0){
				if(antecedent->getX() - antecedent->get_antecedent()->getX() != 0){
					coeff_dir = (double)((double)antecedent->getY() * 1.0 - (double)antecedent->get_antecedent()->getY()) / ((double)antecedent->getX() - (double)antecedent->get_antecedent()->getX());
// 					std::cout << taille << std::endl;
					if(coeff_dir_prec != 0.0)
						if(coeff_dir == coeff_dir_prec && !vertical && !horizontal){
							if( antecedent2->get_antecedent()->antecedent_exist() ){
								antecedent2->set_antecedent(antecedent2->get_antecedent()->get_antecedent());
								taille--;
								cont = true;
							}
						}
					coeff_dir_prec = coeff_dir;
					vertical = false;
					horizontal = false;
				}
				else{
					if(antecedent->getX() - antecedent->get_antecedent()->getX() == 0){
						if(horizontal){
							if( antecedent2->get_antecedent()->antecedent_exist() ){
								antecedent2->set_antecedent(antecedent2->get_antecedent()->get_antecedent());
								taille--;
								cont = true;
							}
						}
						horizontal = true;
						vertical = false;
					}
				}
			}
			else{
				if(antecedent->getY() - antecedent->get_antecedent()->getY() == 0){
					if(vertical){
						if( antecedent2->get_antecedent()->antecedent_exist() ){
							antecedent2->set_antecedent(antecedent2->get_antecedent()->get_antecedent());
							taille--;
							cont = true;
						}
					}
					vertical = true;
					horizontal = false;
				}
			}
			if(cpt != 0)
				antecedent2 =antecedent2->get_antecedent();
			antecedent = antecedent->get_antecedent();
			cpt++;
		}
	}	
}


