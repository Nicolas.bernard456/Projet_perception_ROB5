#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include "Noeud.hpp"
#include "graph.hpp"
#include "nav_msgs/Odometry.h"
#include <commande/Twist_ordre.h>
#include <nav_msgs/Path.h>
#include <tf/transform_listener.h>
#include "tf/message_filter.h"
#include <tf/transform_datatypes.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Bool.h>
// #include <boost/graph/adjacency_list.hpp>
ros::Publisher pub, pub_path, pub_wait_commande,pub_debug;

cv::Mat image, old_image;;
bool display;
int param_div;
int taille_init;

geometry_msgs::PointStamped depart, arrive;
int cpt_es, cpt_noeud;;	//Compteur entrée sortie et compteur de noeud.
nav_msgs::OccupancyGrid new_map_temp;
Graph g;
std::string pcc;
int extremum[4] = {-1,-1,-1,-1};	//Les variables extremum permettent d'optimiser l'execution du code. On efectue le traitement uniquement entre les bornes min et max de x et y(calculé à l'init de la carte)
float rayon_robot;
bool first_map = true, maj_map;
nav_msgs::OccupancyGrid new_map;
ros::Time temps;
nav_msgs::Path path, path_dest;


//Fonction transformant une position d'un repere vers un autre
geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in){
	geometry_msgs::PoseStamped p_out;
// 	try{
		ros::Time t = ros::Time(0);
		tf::TransformListener tr;
		tr.setUsingDedicatedThread(true);
		tr.waitForTransform("/map", "/odom", t,ros::Duration(0.5));
		tr.transformPose("map", p_in, p_out);
// 		ROS_WARN("TF OK");
// 	}catch(tf::TransformException& ex){ 	    
// 		ROS_ERROR("Error %s",ex.what());
// 		ROS_ERROR("Erreur de frame");
// 	}
		
	return p_out;
		
}

bool obstacle(std::vector<int> map){
	for(int i = 0 ; i < map.size(); i++){
		if(map[i] != 0)
			return false ;
	}
	return true;
}



void change_frame_path(){
	path_dest.poses.clear();
	path_dest.header.frame_id = new_map.header.frame_id;
	path_dest.header.stamp = new_map.header.stamp;
	for(int i = 0 ; i < path.poses.size() ; i++){
		geometry_msgs::PoseStamped pose;
		pose.pose.position.x = path.poses[i].pose.position.x * new_map.info.resolution + new_map.info.origin.position.x;
		pose.pose.position.y = path.poses[i].pose.position.y * new_map.info.resolution + new_map.info.origin.position.y;
		path_dest.poses.push_back(pose);
	}
}

nav_msgs::OccupancyGrid init_map_image(nav_msgs::OccupancyGrid map){
	nav_msgs::OccupancyGrid new_map;
	new_map.header = map.header;
	new_map.info.origin = map.info.origin;
	new_map.header.frame_id = map.header.frame_id;
	new_map.info.width = map.info.width;
	new_map.info.height = map.info.height;
	new_map.info.resolution = map.info.resolution;
	new_map.data.clear();
	new_map.data.assign(new_map.info.height * new_map.info.width,-1);
	FILE* out;
	std::string mapdatafile = "test.pgm";
	out = fopen(mapdatafile.c_str(), "w");
	if (!out || map.data.size() == 0){
		std::cout << "Couldn't save map file to %s" << std::endl;
		return new_map;
	}

	fprintf(out, "P5\n# CREATOR: Map_generator.cpp %.3f m/pix\n%d %d\n255\n", map.info.resolution, map.info.width, map.info.height);
	for(unsigned int y = 0; y < map.info.height; y++) {
		for(unsigned int x = 0; x < map.info.width; x++) {
			unsigned int i = x + (map.info.height - y - 1) * map.info.width;	//Le repere part d'en bas a gauche sur la carte alors que l'image part d'en haut a gauche
			if (map.data[i] < 0 || map.data[i] == 100)
				new_map.data[i] = 100;
			else{
				//Optimisation, on regarde quand commence la carte pour éviter de passer sur des zones inutiles
				if(extremum[0] > x || extremum[0] == -1)
					extremum[0] = x;
				if(extremum[2] < x || extremum[2] == -1)
					extremum[2] = x;
				if(extremum[1] > map.info.height - y - 1 || extremum[1] == -1)
					extremum[1] = map.info.height - y - 1;
				if(extremum[3] < map.info.height - y - 1 || extremum[3] == -1)
					extremum[3] = map.info.height - y - 1;
				new_map.data[i] = 0;
			}

		}
	}
	if(extremum[0] - 2*param_div >= 0)
		extremum[0] -= 2*param_div;
	
	if(extremum[1] - 2*param_div >= 0)
		extremum[1] -= 2*param_div;
	
	if(extremum[2] + 2*param_div < map.info.width)
		extremum[2] += 2*param_div;
	if(extremum[3] + 4*param_div < map.info.height)
		extremum[3] += 4*param_div;
	int bordure = rayon_robot / (float)new_map.info.resolution;
	for ( int y = extremum[1] ; y < extremum[3] ; y++){
		for (int x = extremum[0] ; x < extremum[2] ; x++){
			unsigned int j = x + (map.info.height - y - 1) * map.info.width;	//Le repere part d'en bas a gauche sur la carte alors que l'image part d'en haut a gauche
			if (map.data[j] == 100){
				for(int x1 = x - bordure ; x1 < x + bordure ; x1++)
					for(int y1 = y - bordure ; y1 < y + bordure ; y1++){
						unsigned int k = x1 + (map.info.height - y1 - 1) * map.info.width;	//Le repere part d'en bas a gauche sur la carte alors que l'image part d'en haut a gauche
						if(k < map.info.height * map.info.width)
							new_map.data[k] = 100;
					}
			}
		}
	}
	for(unsigned int y = 0; y < map.info.height; y++) {
		for(unsigned int x = 0; x < map.info.width; x++) {
		unsigned int i = x + (map.info.height - y - 1) * map.info.width;	//Le repere part d'en bas a gauche sur la carte alors que l'image part d'en haut a gauche
		if (new_map.data[i] == 0)
			fputc(254, out);
		else if (new_map.data[i] == +100) 
			fputc(000, out);
		else  
			fputc(205, out);
		}
	}
	fclose(out);
	std::cout << "map saved" << std::endl;
	image = cv::imread("test.pgm", CV_LOAD_IMAGE_COLOR);   // Read the file
// 	cv::imshow( "Dilation Demo", image );
// 	
// 	
// 	//Dilatation de l'image pour supprimer les points non detecté
	cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,cv::Size(10,10),cv::Point( 2, 2 ) );
	cv::dilate(image,image,element);
// 	cv::imshow( "Erosion Demo", image );
// 	cv::waitKey(0.0);
	
	//Corection new map avec dilatation
	cv::Mat img_copy = image.clone();
	cv::cvtColor( image, img_copy, CV_BGR2GRAY );
	for(unsigned int y = extremum[1]; y < extremum[3]; y++) {
		for(unsigned int x = extremum[0]; x < extremum[2]; x++) {
			unsigned int i = x + (map.info.height - y - 1) * map.info.width;	//Le repere part d'en bas a gauche sur la carte alors que l'image part d'en haut a gauche
			unsigned int j = x + y * map.info.width;	//Le repere part d'en bas a gauche sur la carte alors que l'image part d'en haut a gauche
			if((int)img_copy.at<uchar>(j) == 0)
				new_map.data[i] = 100;
			else
				new_map.data[i] = 0;
		}
	}
	if(! image.data ){
		std::cout <<  "Could not open or find the image" << std::endl ;
		return new_map;
	}
	return new_map;
}

void callback(nav_msgs::OccupancyGrid map_){
	std::cout << "Map_received" << std::endl;
	//Init du noeud ros dans la classe Graph
	int i = 0, x2 = 0,y2 = 0;
	std::vector<int> temp;
	bool obstacle_map;
	//On crée l'image de la map et on init les min et max de x et y (optimisation)
// 	if(first_map == true){
	if(ros::Time::now() - temps > ros::Duration(1.0))
		maj_map = true;
	
	
	
	if(maj_map){
		new_map = init_map_image(map_);
		first_map = false;
		
		for(i = 0 ; i < 4 ; i++)
			if(extremum[i] == -1){
				ROS_ERROR("Erreur lors de la création de la carte, extremum non initialisé");
				return ; //Si les extremum ne sont pas initialisé, il y a eu un probleme. On arrete le prgm
			}
		i = 0;
		g.reset();
		cpt_noeud = 0;
		//On observe la map entre xmin ymin et max ymax
		for ( int y = extremum[1] ; y < extremum[3] ; y = y + param_div){
			for (int x = extremum[0] ; x < extremum[2] ; x = x + param_div){
				for (x2 = x ; x2 < x + param_div ; x2 ++)
					for (y2 = y ; y2 < y + param_div ; y2 ++)
						temp.push_back(new_map.data[(new_map.info.width)*y2 + x2]); //On segmente la map en carrée de surface paramdiv^2
				if(temp.size() == (param_div * param_div))	//On vérifie que les carrées font bien la taille demandé
					obstacle_map = obstacle(temp);
				else
					obstacle_map = false;
				if(obstacle_map){	//Si la taille correspond on continue
					Noeud n;
					n.init_noeud(cpt_noeud++,temp,param_div);	//Ajout d'un noeud avec son numero, ces pixel et le parametre de division utilisé
					n.ini_centre(x2,y2);	//On definit le noeud avec son premier pixel
					g.Add_noeud(n);	///Ajout du noeud au graph
				}
				temp.clear();	//On nettoie le tableau qui stock les piel d'un noeud
			}
		}
		std::cout << "Attente creation connexion entre noeuds" << std::endl;
		g.init_connexion(&image);
		std::cout << "Fin creation connexions" << std::endl;
		new_map_temp = new_map;
		std::cout << "Map envoyee Taille du graph:" << g.get_size() <<  std::endl;
		taille_init = g.get_size();
		pub.publish(new_map);
	}
	else{
		new_map.header = map_.header;
		change_frame_path();
		pub_path.publish(path_dest);
	}
}

void callback2(geometry_msgs::PointStamped ps){
	geometry_msgs::PoseStamped p_out;
	geometry_msgs::PoseStamped p_in;
	
	p_in.pose.orientation.x = 0.0;
	p_in.pose.orientation.y = 0.0;
	p_in.pose.orientation.z = 0.0;
	p_in.pose.orientation.w = 1.0;
	p_in.header.frame_id = ps.header.frame_id;
	p_in.pose.position = ps.point;
	p_out = transfo_map_odom(p_in);
	geometry_msgs::PointStamped point_out;
	point_out.header.frame_id = p_out.header.frame_id;
	point_out.point = p_out.pose.position;
	pub_debug.publish(point_out);
	int x = (p_out.pose.position.x - new_map_temp.info.origin.position.x)/new_map_temp.info.resolution;
	int y = (p_out.pose.position.y - new_map_temp.info.origin.position.y)/new_map_temp.info.resolution;	
	if(new_map_temp.data[(new_map_temp.info.width)*y + x] == 0){
		if(old_image.rows != 0){
			image = old_image.clone();
			cv::destroyAllWindows();
		}
		std::cout << "Position d'arrivee recue" << std::endl;
		std_msgs::Bool wait_com;
		wait_com.data = true;
		pub_wait_commande.publish(wait_com);
		cpt_es++;
		arrive = ps;
		std::cout << "Ajout des nouveaux noeuds" << std::endl;
		Noeud n;
		int id_noeud_dep = cpt_noeud++;
		n.init_noeud_es(id_noeud_dep ,(depart.point.x - new_map_temp.info.origin.position.x)/new_map_temp.info.resolution,(depart.point.y - new_map_temp.info.origin.position.y)/new_map_temp.info.resolution,param_div);
		g.Add_noeud(n);
		Noeud n2;
		int id_noeud_arr = cpt_noeud++;
		n2.init_noeud_es(id_noeud_arr,(arrive.point.x - new_map_temp.info.origin.position.x)/new_map_temp.info.resolution,(arrive.point.y - new_map_temp.info.origin.position.y)/new_map_temp.info.resolution,param_div);
		g.Add_noeud(n2);
		std::cout << "display image" << std::endl;
		if(!g.init_connexion2(&image,taille_init) || g.getNoeud(id_noeud_dep).get_adjac().size() == 0 || g.getNoeud(id_noeud_arr).get_adjac().size() == 0){
			ROS_ERROR("Erreur lors de la creation des noeuds, certains noeuds sont isoles");
			ROS_WARN("Reexecuter l'algo avec un pas plus faible");
			std::cout << g.getNoeud(id_noeud_dep).get_adjac().size() << g.getNoeud(id_noeud_arr).get_adjac().size() << std::endl;
			wait_com.data = false;
			pub_wait_commande.publish(wait_com);
			g.getNoeud(cpt_noeud-1).display(&image);
			return ;
		}
		old_image = image.clone();	//On save l'image avec les noeuds pour la réutiliser ensuite sans redefinir l'ensemble des noeuds
	
		if(strcmp(pcc.c_str(),"Astar") == 0){
			path.poses.clear();
			path = g.Astar(display,new_map_temp,id_noeud_dep, id_noeud_arr, &image);
			if(path.poses.size() == 0){
				wait_com.data = false;
				pub_wait_commande.publish(wait_com);
				ROS_ERROR("Erreur lors de la recherche du plus court chemin");
				ROS_WARN("Le point selectionne est trop proche d'un obstacle");
				return ;
			}
		}
		else{
			path.poses.clear();
			path = g.dijkstra(display,new_map_temp,id_noeud_dep, id_noeud_arr, &image);
			if(path.poses.size() == 0){
				wait_com.data = false;
				pub_wait_commande.publish(wait_com);
				ROS_ERROR("Erreur lors de la recherche du plus court chemin");
				ROS_WARN("Le point selectionne est trop proche d'un obstacle");
				return ;
			}
			
		}
		change_frame_path();
		pub_path.publish(path_dest);
		if(display)
			g.getNoeud(id_noeud_arr).display(&image);
		wait_com.data = false;
		pub_wait_commande.publish(wait_com);
	}
	else{
			std::cout << "Position d'arrivee trop proche d'un obstacle ou sur un obstacle" << std::endl;
			g.getNoeud(cpt_noeud-1).display(&image);
	}

}





void getposrobot(nav_msgs::Odometry odom){
	geometry_msgs::PoseStamped p_out;
	geometry_msgs::PoseStamped p_in;
	
	p_in.pose.orientation.x = 0.0;
	p_in.pose.orientation.y = 0.0;
	p_in.pose.orientation.z = 0.0;
	p_in.pose.orientation.w = 1.0;
	p_in.header.frame_id = odom.header.frame_id;
	p_in.pose.position = odom.pose.pose.position;
	p_out = transfo_map_odom(p_in);
	
	geometry_msgs::PointStamped p;
	p.header.frame_id = "map";
	p.point.x = p_out.pose.position.x;
	p.point.y = p_out.pose.position.y;
	depart = p;
}

void get_situation(commande::Twist_ordre t){
	maj_map = !t.active;
	temps = ros::Time::now();
}

int main(int argc, char ** argv){
	ros::init(argc, argv, "test_map");
	ros::NodeHandle n , nh("~");
	nh.param<bool>("display",display,true);
	nh.param<int>("param_div",param_div,25);
	nh.param<float>("rayon_robot",rayon_robot,0.050);
	nh.param<std::string>("Pcc",pcc,"Astar"); //On definit la methode de plus court chemin par defaut a dikjstra. On peu aussi choisir Astar. puis RRT et la méthode par les champs de potentiel(pas encore implem)
	if(display)
		std::cout << "Display on" << std::endl;
	else
		std::cout << "Display off" << std::endl;
	
// 	cv::namedWindow( "Erosion Demo", CV_WINDOW_NORMAL );
// 	cv::namedWindow( "Dilation Demo", CV_WINDOW_NORMAL );
	std::cout << "Segmentation de la map en carree de " << param_div << " pixels." << std::endl;
	std::cout << "Méthode de parcours utilisé : " << pcc << std::endl;
	std::cout << "Rayon du robot : " << rayon_robot << std::endl;
	cpt_es = 0;
	cpt_noeud = 0;
	maj_map = true;
	ros::Subscriber sub = n.subscribe("/map",1 ,callback);
	ros::Subscriber sub2 = n.subscribe("/clicked_point",1 ,callback2);
	ros::Subscriber sub3 = n.subscribe("/odom",1 ,getposrobot);
	ros::Subscriber sub_commande = n.subscribe("/cmd_vel_ordre_1",1 ,get_situation);
	pub_path = n.advertise<nav_msgs::Path>("/Waypoints_robot", 1);
	pub = n.advertise<nav_msgs::OccupancyGrid>("/new_map", 1);
	pub_wait_commande = n.advertise<std_msgs::Bool>("/wait_commande",1);
	pub_debug = n.advertise<geometry_msgs::PointStamped>("/point_cible",1);
	
	ros::Rate loop(100);
	while(ros::ok()){
		loop.sleep();
		ros::spinOnce();
	}
	return 0;
}