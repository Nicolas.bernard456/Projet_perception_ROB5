#include "map.hpp"

nav_msgs::OccupancyGrid Map::init_map_image(nav_msgs::OccupancyGrid map, int* coord_extremum, bool display, cv::Mat Image){
	nav_msgs::OccupancyGrid new_map;
	new_map.header = map.header;
	new_map.info.origin = map.info.origin;
	new_map.header.frame_id = map.header.frame_id;
	new_map.info.width = map.info.width;
	new_map.info.height = map.info.height;
	new_map.info.resolution = map.info.resolution;
	new_map.data.assign(new_map.info.height * new_map.info.width,-1);
	FILE* out;
	if(display){
		std::string mapdatafile = "test.pgm";
		out = fopen(mapdatafile.c_str(), "w");
		if (!out || map.data.size() == 0){
			std::cout << "Couldn't save map file to %s" << std::endl;
			return new_map;
		}
	
	fprintf(out, "P5\n# CREATOR: Map_generator.cpp %.3f m/pix\n%d %d\n255\n", map.info.resolution, map.info.width, map.info.height);
	}
	for(unsigned int y = 0; y < map.info.height; y++) {
		for(unsigned int x = 0; x < map.info.width; x++) {
			unsigned int i = x + (map.info.height - y - 1) * map.info.width;	//Le repere part d'en bas a gauche sur la carte alors que l'image part d'en haut a gauche
			if (map.data[i] < 0)
				new_map.data[i] = 100;
			else if (map.data[i] == 100)
				new_map.data[i] = 100;
			else{
				//Optimisation, on regarde quand commence la carte pour éviter de passer sur des zones inutiles
				if(coord_extremum[0] > x || coord_extremum[0] == -1)
					coord_extremum[0] = x;
				else if(coord_extremum[2] < x || coord_extremum[2] == -1)
					coord_extremum[2] = x;
				if(coord_extremum[1] > map.info.height - y - 1 || coord_extremum[1] == -1)
					coord_extremum[1] = map.info.height - y - 1;
				else if(coord_extremum[3] < map.info.height - y - 1 || coord_extremum[3] == -1)
					coord_extremum[3] = map.info.height - y - 1;
				new_map.data[i] = 0;
			}
			if(display){
				if (map.data[i] == 0) { //occ [0,0.1)
					fputc(254, out);
				} else if (map.data[i] == +100) { //occ (0.65,1]
					fputc(000, out);
				} else { //occ [0.1,0.65]
					fputc(205, out);
				}
			}
		}
	}
	if(display){
		fclose(out);
		std::cout << "map saved" << std::endl;
		image = cv::imread("test.pgm", CV_LOAD_IMAGE_COLOR);   // Read the file
		if(! image.data ){
			std::cout <<  "Could not open or find the image" << std::endl ;
			return new_map;
		}
	}
	return new_map;
}