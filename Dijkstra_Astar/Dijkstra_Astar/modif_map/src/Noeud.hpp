#ifndef NOEUD_HPP
#define NOEUD_HPP

#include <stdlib.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <nav_msgs/OccupancyGrid.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>

class Noeud {
	private : 
		std::vector<int> data;
		int numero_noeud;
		int x_centre,y_centre;
		std::vector<Noeud*> adjac; // Noeud adjacent
		Noeud* antecedent;
		int size_pix;
	public :
		Noeud();
		~Noeud();
		void init_noeud(int numero_noeud_, std::vector<int> data_, int size_pix_);
		void init_noeud_es(int numero_noeud_, int x_centre_, int y_centre_, int size_pix_ );
		void ini_centre(int x_centre_, int y_centre_);
		void create_output(cv::Mat* image);
		int get_num_noeud();
		void display(cv::Mat *image_);
		int getX();
		int getY();
		bool check_adjac(Noeud* Noeud_adj, cv::Mat *image);
		std::vector<Noeud*> get_adjac();
		Noeud* get_antecedent();
		void set_antecedent(Noeud* antecedent_);
		void color_chemin(cv::Mat *image_);
		bool antecedent_exist();
		bool check_adjac_es(Noeud* Noeud_adj, cv::Mat *image);
		bool check_adjac_es2(Noeud* Noeud_adj, cv::Mat* image);
		void clear_antec();
		void clear_adjac();
};




#endif