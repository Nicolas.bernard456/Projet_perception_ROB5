Projet de robotique mobile ROB5 POLYTECH PARIS UPMC

Membres : BERNARD Nicolas, GUENERS Damien, KUN Ludovic

Pour lancer le programme en simulation :

    Executer le launcher gazebo avec le robot minilab + gmapping
    
    Executer "roslaunch commande commande_evitement_mapping.launch"
    
    Publier avec rostopic pub /batterie std_msgs/Bool "data: true" pour dire au robot que sa batterie est ok
    
Pour lancer le programme sur le robot Pionneer:
    
    Executer sur le robot roslaunch pionneer pionneer.launch
    
    Executer "roslaunch commande commande_evitement_mapping_pioneer.launch"
    
    Attendre la construction d'une bonne map
    
    Publier un point sur Rviz avec Publish Point le robot effectuera la commande