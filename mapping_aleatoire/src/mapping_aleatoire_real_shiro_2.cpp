#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <iostream>
#include <commande/Twist_ordre.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Twist.h>
#include <time.h>
//#include <boost/graph/graph_concepts.hpp>
// On controle la vitesse du robot
ros::Publisher move_publisher;
ros::Subscriber sensor_subscriber;
ros::Subscriber sensor_subscriber2;
ros::Subscriber sensor_subscriber3;
ros::Subscriber sensor_subscriber4;
ros::Subscriber sensor_subscriber5;

// Topic pour publier la position intiale et finale de notre robot
ros::Publisher publisher_posInit;
ros::Publisher publisher_posFinal;
ros::Subscriber subs_posInit;
ros::Subscriber subs_posFinal;

geometry_msgs::PointStamped pose_init;
geometry_msgs::PointStamped pose_final;

bool b_init ;
bool b_final ;
//
//geometry_msgs::Twist mouvement;// Vitesse robot
//commande::Twist_ordre mouvement;
geometry_msgs::Twist mouvement;

double PI = 3.14159265359;
ros::Time t;
bool decoincement = false;
int compteur_detection = 0;
bool Laser[8] = {false};
double LaserPortee = 0.5;

double aleatoire(double min, double max){
  srand(time(NULL));
  double num = ( ((double)rand()*(max-min)/RAND_MAX)-min);
  return num;
  
}

void initMove( geometry_msgs::Twist& m, bool vitesse_null, double vitesse){
    m.angular.x = 0;
    m.angular.y = 0;
    m.angular.z = 0;
    m.linear.y = 0;
    m.linear.z = 0;
    if(vitesse_null == false){
      m.linear.x = 0.0;
      m.angular.z = PI/10;
    }
    else{
      m.linear.x = vitesse;
      
    }
}

void Move(geometry_msgs::Twist& m){
    m.angular.x = 0;
    m.angular.y = 0;
    m.angular.z = 0;
    m.linear.x = 0;
    m.linear.y = 0;
    m.linear.z = 0;
    std::cout << Laser[0] << " " << Laser[1] << " " << Laser[2] << " " << Laser[3] << " " << Laser[4] << std::endl;
  /*if(Laser[0]==false && Laser[1] == false && Laser[2] == false && Laser[3] == false && Laser[4] == false){
    m.t.linear.x = 0.5;
  }
  else if(Laser[0]==true && Laser[1] == true && Laser[2] == true && Laser[3] == true && Laser[4] == true){
     m.t.angular.z = PI/5;
   
  }
  else if( (Laser[0] == true || Laser[1] ==true) && (Laser[3] == false && Laser[4] == false)){
    m.t.angular.z = -PI/10;
    //m.angular.z = -aleatoire(0.0,PI)/2;
  }
  else if( (Laser[3] == true || Laser[4] ==true) && (Laser[1] == false && Laser[2] == false)){
    m.t.angular.z = PI/10;
     //m.angular.z = aleatoire(0.0,PI)/2;
    }else{
    m.t.angular.z = PI/5; 
  }*/
  bool droite = false, gauche = false;
  for(int i = 0; i < 4 ; i++){
    if(Laser[i] == true){
     droite = true;
     break;
    }
  }
  
  for(int i = 4; i < 8 ; i++){
    if(Laser[i] == true){
     gauche = true;
     break;
    }
  }
  
  if( gauche == false && droite == false){
    m.linear.x = 0.3;
  }
  else if(gauche == true && droite == true){
     m.angular.z = PI/5;
  }
  else if( gauche ==true && droite == false){
     m.angular.z = -PI/10;
  }
  else{
     m.angular.z = PI/10;
  }
}
/*
void sensor_callback(sensor_msgs::LaserScan data){
	if(decoincement){
		if( ros::Time::now() - t > ros::Duration(1.0))
			decoincement = false;
		std::cout << t - ros::Time::now() << std::endl;
		initMove(mouvement, false, 0.0);
		mouvement.t.linear.x = 0.0;
		mouvement.t.angular.z = 1.2;
		move_publisher.publish(mouvement); // On publie la vitesse du robot
	}
	else{
		int g1 = 0, g2 = 0, d1 = 0 , d2 = 0;
	// 	ROS_INFO("Nuage recu");
		initMove(mouvement, false, 0.0);
		
		for(int i = 0 ; i < data.ranges.size() ; i ++){
			if(data.ranges[i] < 1.0){
				if(i < (data.ranges.size())/4) 
					d1++;
				else if(i < 2*(data.ranges.size())/4) 
					d2++;
				else if(i < 3*(data.ranges.size())/4) 
					g1++;
				else if(i < 4*(data.ranges.size())/4) 
					g2++;
// 				std::cout << (data.ranges.size())/4 << std::endl;
			}
		}
		mouvement.t.linear.x = 0.3;
		mouvement.t.angular.z = 0.0;
		std::cout << g1 << " " << g2 << " " << d1 << " " << d2 << std::endl;
		std::cout << (g1 + g2) - (d1 + d2) << std::endl;

		if((g1 + g2) - (d1 + d2) < 100 && (g1 + g2) - (d1 + d2) != 0){
			mouvement.t.linear.x = 0;
			mouvement.t.angular.z = 1.0;
			if(g1 < g2){
				mouvement.t.linear.x = 0.3;
				mouvement.t.angular.z = 0.5;
			}
			compteur_detection ++;
		}
		else{
			if((g1 + g2) - (d1 + d2) > 100){
				mouvement.t.linear.x = 0;
				mouvement.t.angular.z = -1.0;
				if(d1 < d2){
					mouvement.t.linear.x = 0.3;
					mouvement.t.angular.z = -0.5;
				}
				compteur_detection++;
			}
			else 
				compteur_detection = 0;
		}
		
// 		if((g1 + g2) - (d1 + d2) < 100 && (g1 + g2) - (d1 + d2) != 0){
// 			mouvement.linear.x = -0.6;
// 			mouvement.angular.z = 1.2;
// 			if(g1 < g2){
// 				mouvement.linear.x = 0.3;
// 				mouvement.angular.z = 0.6;
// 			}
// 			compteur_detection ++;
// 		}
// 		else{
// 			if((g1 + g2) - (d1 + d2) > 100){
// 				mouvement.linear.x = 0.2;
// 				mouvement.angular.z = -0.8;
// 				if(d1 < d2){
// 					mouvement.linear.x = 0.3;in
// 					mouvement.angular.z = -0.4;
// 				}
// 				compteur_detection++;
// 			}
// 			else 
// 				compteur_detection = 0;
// 		}	
		if(compteur_detection > 20){
			decoincement = true;
			compteur_detection = 0;
			t = ros::Time::now();
		}
		
		
	// 	if( g < d ){
	// 		mouvement.linear.x = -0.1;
	// 		mouvement.angular.z = 1.0;
	// 	}
	// 	else if(d < g){
	// 		mouvement.linear.x = -0.1;
	// 		mouvement.angular.z = -0.8;
	// 
	// 	}
	// 	else{
	// 		mouvement.linear.x = 0.8;
	// 		mouvement.angular.z = 0.0;
	// 	}

		move_publisher.publish(mouvement); // On publie la vitesse du robot
	}
}
*/

void sensor_callback_simu_1(sensor_msgs::PointCloud data){
  //std::cout << "laser 1 mise à jour" << std::endl;
  for(int i = 1; i <= 6 ; i++){
      float intensity = data.points[i].x;
      if(intensity < LaserPortee){
	Laser[i] = true;
      }else{
	Laser[i] = false;
      }
  
    
  }
}
/*void sensor_callback_simu_2(sensor_msgs::PointCloud data){
float intensity = data.points.x;
  if(intensity < LaserPortee- 0.01){
    Laser[1] = true;
  }else{
    Laser[1] = false;
  }
}

void sensor_callback_simu_3(sensor_msgs::PointCloud data){
  //std::cout << "laser 3 mise à jour" << std::endl;
float intensity = data.points.x;
  if(intensity < LaserPortee- 0.01){
    Laser[2] = true;
  }else{
    Laser[2] = false;
  }
}

void sensor_callback_simu_4(sensor_msgs::PointCloud data){
float intensity = data.points.x;
  if(intensity < LaserPortee- 0.01){
    Laser[3] = true;
  }else{
    Laser[3] = false;
  }
}

void sensor_callback_simu_5(sensor_msgs::PointCloud data){
float intensity = data.points.x;
  if(intensity < LaserPortee- 0.01){
    Laser[4] = true;
  }else{
    Laser[4] = false;
  }
}*/

void init_callback(nav_msgs::Odometry data){
  b_init = true;
  pose_init.point = data.pose.pose.position;
  pose_init.header.frame_id = data.header.frame_id;
}

void final_callback(nav_msgs::Odometry data){
  b_final = true;
  pose_final.point = data.pose.pose.position;
  pose_final.header.frame_id = data.header.frame_id;
  
}

void subscriber_pose_init(ros::NodeHandle n){ // Odometry
  b_init = false;
  subs_posInit = n.subscribe("odom",100, &init_callback);
  //subs_posInit = n.subscribe("rosaria_driver/pose",100, &init_callback);
  ros::Rate r(10);
  //cout << "avant while " << BoolMap << endl;
  while(n.ok() && b_init == false){
   // cout << " dans la boucle" << endl;
    ros::spinOnce();
    r.sleep();
  }
  
 subs_posInit.shutdown();
 std::cout << "position intiale recup" << std::endl;

  
}

void subscriber_pose_final(ros::NodeHandle n){
  b_final = false;
  subs_posFinal = n.subscribe("odom",100, &final_callback);
  //subs_posFinal = n.subscribe("rosaria_driver/pose",100, &final_callback);
  ros::Rate r(10);
  //cout << "avant while " << BoolMap << endl;
  while(n.ok() && b_final == false){
   // cout << " dans la boucle" << endl;
    ros::spinOnce();
    r.sleep();
  }
  
  subs_posFinal.shutdown();
  std::cout << "position final recupéré" << std::endl;
}


int main(int argc, char** argv){
	ros::init(argc, argv, "mapping_aleatoire_real_shiro_2");
	ros::NodeHandle n; // declaration noeud
	ros::Time t, t0, now;
	bool time = false;
	t = ros::Time::now();
	//mouvement.active = true;
	subscriber_pose_init(n);
	std::cout << " X init = " << pose_init.point.x << " Y init = " << pose_init.point.y << std::endl;
	
	// Declaration publisher / Subscriber pour les lasers
	move_publisher = n.advertise<geometry_msgs::Twist>("/cmd_vel",10);// creation d'un publisher pour le topic cmd_vel
	sensor_subscriber = n.subscribe<sensor_msgs::PointCloud>("/RosAria/sonar", 1000, sensor_callback_simu_1);// bumper_state
	/*sensor_subscriber2 = n.subscribe<sensor_msgs::LaserScan>("/IR2", 1000, sensor_callback_simu_2);// bumper_state
	sensor_subscriber3 = n.subscribe<sensor_msgs::LaserScan>("/IR3", 1000, sensor_callback_simu_3);// bumper_state
	sensor_subscriber4 = n.subscribe<sensor_msgs::LaserScan>("/IR4", 1000, sensor_callback_simu_4);// bumper_state
	sensor_subscriber5 = n.subscribe<sensor_msgs::LaserScan>("/IR5", 1000, sensor_callback_simu_5);// bumper_state*/
	
	ros::Rate loop_rate(100);
	// Initialisation du mouvement
	initMove(mouvement, true,0.3);
	int count = 0;
	t0 = ros::Time::now();
	while(ros::ok() ){
	  //std::cout <<ros::Time::now()-t0 << std::endl;
	    now = ros::Time::now();
	    move_publisher.publish(mouvement);
	    
	    if(now-t0 > ros::Duration(3.0)){
	      break;
	    }
	    
	    ros::spinOnce();
	    loop_rate.sleep();
	  
	  
	}
	
	
	std::cout << " Initialisation vitesse réalisé " << std::endl;
	// rotation robot

	initMove(mouvement, false,0.0);
	t0 = ros::Time::now();
	while(ros::ok() ){
	  //std::cout <<ros::Time::now()-t0 << std::endl;
	    now = ros::Time::now();
	    move_publisher.publish(mouvement);
	    
	    if(now-t0 > ros::Duration(21.0)){
	      break;
	    }
	    
	    ros::spinOnce();
	    loop_rate.sleep();
	  
	  
	}
	// Deplacement aléatoire du robot
	t0 = ros::Time::now();
	while(ros::ok()){
	  //std::cout <<ros::Time::now()-t0 << std::endl;
	      now = ros::Time::now();
	      
	    if(now-t0 > ros::Duration(50.0)){
	      break;
	    }
	    
	    if( now - t > ros::Duration(0.5)){
	      Move(mouvement);
	      move_publisher.publish(mouvement);
	      time = true;
	      
	      //std:: cout << " Mise à jour vitesse " << std::endl;
	    }
	    else{
	      time = false; 
	    }
	    
	    ros::spinOnce();
	    loop_rate.sleep();
	    
	    if(time == true){
		t = ros::Time::now();
	    }
	  
	}
	initMove(mouvement, true,0.0);
	move_publisher.publish(mouvement);
	subscriber_pose_final(n);
	std::cout << " X final = " << pose_final.point.x << " Y final = " << pose_final.point.y << std::endl;
	
	/* Publication des pos intiales et finales */
	publisher_posInit = n.advertise<geometry_msgs::PointStamped>("PosInit",10);
	
	while(publisher_posInit.getNumSubscribers() == 0){
      
	  loop_rate.sleep();
   
	}
	publisher_posInit.publish(pose_init);
	ros::spinOnce();

	publisher_posFinal =  n.advertise<geometry_msgs::PointStamped>("PosFinal",10);
	while(publisher_posFinal.getNumSubscribers() == 0){
	    
	    loop_rate.sleep();
	
	}
	publisher_posFinal.publish(pose_final);
	ros::spinOnce();
	
	return 0;
}
