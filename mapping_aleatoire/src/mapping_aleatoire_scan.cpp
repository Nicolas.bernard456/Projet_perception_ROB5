#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
// On controle la vitesse du robot
ros::Publisher move_publisher;
ros::Subscriber sensor_subscriber;
geometry_msgs::Twist mouvement;// Vitesse robot
sensor_msgs::PointCloud2 p;
ros::Publisher pc_pub;
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud , cloud2;

ros::Time t;


bool decoincement = false;
int compteur_detection = 0;

void initMove( geometry_msgs::Twist& m){
    m.angular.x = 0;
    m.angular.y = 0;
    m.angular.z = 0;
    m.linear.x = 0.8;
    m.linear.y = 0;
    m.linear.z = 0;
    
  
}

void sensor_callback(sensor_msgs::PointCloud2::Ptr data){
	if(decoincement){
		if( ros::Time::now() - t > ros::Duration(1.0))
			decoincement = false;
		std::cout << t - ros::Time::now() << std::endl;
		initMove(mouvement);
		mouvement.linear.x = 0.0;
		mouvement.angular.z = 1.2;
		move_publisher.publish(mouvement); // On publie la vitesse du robot
	}
	else{
		int g1 = 0, g2 = 0, d1 = 0 , d2 = 0;
	// 	ROS_INFO("Nuage recu");
		initMove(mouvement);
		cloud->clear();
		pcl::fromROSMsg(*data,*cloud);
		cloud2->clear();
		for(int i = 0 ; i < cloud->height * cloud->width ; i ++){
			if(cloud->points[i].z < 2.0 && cloud->points[i].y < 0 ){
				if(cloud->points[i].x > 0 && cloud->points[i].x < 0.5)
					d1++;
				else if(cloud->points[i].x >= 0.5)
					d2++;
				if(cloud->points[i].x < 0 && cloud->points[i].x > -0.5)
					g1++;
				else if(cloud->points[i].x <= -0.5)
					g2++;
				cloud2->push_back(cloud->points[i]);
	// 			std::cout << cloud->points[i].z << std::endl;
			}
	// 		std::cout << cloud->points[i].y << cloud->points[i].x << cloud->points[i].z << std::endl;
		}
		mouvement.linear.x = 0.5;
		mouvement.angular.z = 0.0;
// 		std::cout << g1 << " " << g2 << " " << d1 << " " << d2 << std::endl;
// 		std::cout << (g1 + g2) - (d1 + d2) << std::endl;

		if((g1 + g2) - (d1 + d2) < 100 && (g1 + g2) - (d1 + d2) != 0){
			mouvement.linear.x = 0;
			mouvement.angular.z = 1.0;
			if(g1 < g2){
				mouvement.linear.x = 0.3;
				mouvement.angular.z = 0.5;
			}
			compteur_detection ++;
		}
		else{
			if((g1 + g2) - (d1 + d2) > 100){
				mouvement.linear.x = 0;
				mouvement.angular.z = -1.0;
				if(d1 < d2){
					mouvement.linear.x = 0.3;
					mouvement.angular.z = -0.5;
				}
				compteur_detection++;
			}
			else 
				compteur_detection = 0;
		}
		
// 		if((g1 + g2) - (d1 + d2) < 100 && (g1 + g2) - (d1 + d2) != 0){
// 			mouvement.linear.x = -0.6;
// 			mouvement.angular.z = 1.2;
// 			if(g1 < g2){
// 				mouvement.linear.x = 0.3;
// 				mouvement.angular.z = 0.6;
// 			}
// 			compteur_detection ++;
// 		}
// 		else{
// 			if((g1 + g2) - (d1 + d2) > 100){
// 				mouvement.linear.x = 0.2;
// 				mouvement.angular.z = -0.8;
// 				if(d1 < d2){
// 					mouvement.linear.x = 0.3;
// 					mouvement.angular.z = -0.4;
// 				}
// 				compteur_detection++;
// 			}
// 			else 
// 				compteur_detection = 0;
// 		}	
		if(compteur_detection > 20){
			decoincement = true;
			compteur_detection = 0;
			t = ros::Time::now();
		}
		
		
	// 	if( g < d ){
	// 		mouvement.linear.x = -0.1;
	// 		mouvement.angular.z = 1.0;
	// 	}
	// 	else if(d < g){
	// 		mouvement.linear.x = -0.1;
	// 		mouvement.angular.z = -0.8;
	// 
	// 	}
	// 	else{
	// 		mouvement.linear.x = 0.8;
	// 		mouvement.angular.z = 0.0;
	// 	}

		move_publisher.publish(mouvement); // On publie la vitesse du robot
		cloud2->header.frame_id = cloud->header.frame_id;
		pc_pub.publish(*cloud2);
	}
}

int main(int argc, char** argv){
	ros::init(argc, argv, "mapping_aleatoire_scan");
	ros::NodeHandle n; // declaration noeud
	cloud = boost::shared_ptr< pcl::PointCloud<pcl::PointXYZ> >(new pcl::PointCloud<pcl::PointXYZ>);
	cloud2 = boost::shared_ptr< pcl::PointCloud<pcl::PointXYZ> >(new pcl::PointCloud<pcl::PointXYZ>);
	pc_pub = n.advertise< pcl::PointCloud<pcl::PointXYZ> >("pcl_seg/points", 10);
	move_publisher = n.advertise<geometry_msgs::Twist>("/cmd_vel",10);// creation d'un publisher pour le topic cmd_vel
	sensor_subscriber = n.subscribe<sensor_msgs::PointCloud2::Ptr>("/camera/depth/points",1, sensor_callback);// bumper_state
	ros::spin();
}