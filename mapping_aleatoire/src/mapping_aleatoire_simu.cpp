#include "ros/ros.h"
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>
#include <commande/Twist_ordre.h>
#include <nav_msgs/Odometry.h>
// On controle la vitesse du robot
ros::Publisher move_publisher, pub_origin;
ros::Subscriber sensor_subscriber,ir_sub1,ir_sub2,ir_sub3,ir_sub4,ir_sub5;
commande::Twist_ordre mouvement;// Vitesse robot
ros::Time t;
geometry_msgs::PointStamped origine;

bool decoincement = false;
int compteur_detection = 0;
std::vector<float> ranges;
bool batterie, originie_set, pub_once;
void initMove( commande::Twist_ordre& m){
    m.t.angular.x = 0;
    m.t.angular.y = 0;
    m.t.angular.z = 0;
    m.t.linear.x = 0.5;
    m.t.linear.y = 0;
    m.t.linear.z = 0;
    
  
}

void sensor_callback(){

	int g1 = 0, g2 = 0, d1 = 0 , d2 = 0;
	initMove(mouvement);
	for(int i = 0 ; i < ranges.size() ; i ++){
		std::cout << ranges[i] << std::endl;
		if(ranges[i] < 0.4){
			if(i == 0) 
				d1++;
			else if( i == 1) 
				d2++;
			else if(i == 3) 
				g1++;
			else
				g2++;
		}
	}
	mouvement.t.linear.x = 0.3;
	mouvement.t.angular.z = 0.0;
	std::cout << g1 << " " << g2 << " " << d1 << " " << d2 << std::endl;
	std::cout << (g1 + g2) - (d1 + d2) << std::endl;

	if((g1 + g2) > (d1 + d2) && (g1 + g2) - (d1 + d2) != 0){
		mouvement.t.linear.x = 0;
		mouvement.t.angular.z = 1.0;
	}
	else{
		if((g1 + g2) < (d1 + d2)){
			mouvement.t.linear.x = 0;
			mouvement.t.angular.z = -1.0;
			compteur_detection++;
		}
	}
	mouvement.active = true;

	move_publisher.publish(mouvement); // On publie la vitesse du robot

}

void ir_sub1_callback(sensor_msgs::LaserScan ir){

	ranges[0] = ir.ranges[0];
}

void ir_sub2_callback(sensor_msgs::LaserScan ir){
// 	std::cout <<ir.ranges[0];
	ranges[1] = ir.ranges[0];
}

void ir_sub3_callback(sensor_msgs::LaserScan ir){
	ranges[2] = ir.ranges[0];
}

void ir_sub4_callback(sensor_msgs::LaserScan ir){
	ranges[3] = ir.ranges[0];
}

void ir_sub5_callback(sensor_msgs::LaserScan ir){
	ranges[4] = ir.ranges[0];
}

void batterie_cb(std_msgs::Bool b){
	if(b.data)
		batterie = true;
	else
		batterie = false;	
}

void origin_cb(nav_msgs::Odometry odom){
	originie_set = false;
	origine.header.frame_id = odom.header.frame_id;
	origine.point = odom.pose.pose.position;
}

int main(int argc, char** argv){
	ros::init(argc, argv, "mapping_aleatoire_simu");
	ros::NodeHandle n; // declaration noeud
	move_publisher = n.advertise<commande::Twist_ordre>("/cmd_vel_ordre_0",10);// creation d'un publisher pour le topic cmd_vel
	for(int i = 0 ; i < 5 ; i++)
		ranges.push_back(0.0);
	ir_sub1 = n.subscribe<sensor_msgs::LaserScan>("/IR1",1, ir_sub1_callback);
	ir_sub2 = n.subscribe<sensor_msgs::LaserScan>("/IR2",1, ir_sub2_callback);
	ir_sub3 = n.subscribe<sensor_msgs::LaserScan>("/IR3",1, ir_sub3_callback);
	ir_sub4 = n.subscribe<sensor_msgs::LaserScan>("/IR4",1, ir_sub4_callback);
	ir_sub5 = n.subscribe<sensor_msgs::LaserScan>("/IR5",1, ir_sub5_callback);
	ros::Subscriber sub_origin = n.subscribe<nav_msgs::Odometry>("/odom", 1, origin_cb );
	//sensor_subscriber = n.subscribe<sensor_msgs::LaserScan>("/camera/depth/points",1, sensor_callback);// bumper_state
	pub_origin = n.advertise<geometry_msgs::PointStamped>("clicked_point",1);
	ros::Subscriber sub_batterie = n.subscribe<std_msgs::Bool>("/batterie", 1, batterie_cb);
	originie_set = true;
	ros::Rate loop(100.0);
	while(ros::ok()){
		if(originie_set == false)
			sub_origin.shutdown();
		if(batterie){
			sensor_callback();
			pub_once = true;
		}
		else{
			if(pub_once && pub_origin.getNumSubscribers() != 0){
				pub_origin.publish(origine);
				pub_once = false;
			}
		}
		loop.sleep();
		ros::spinOnce();
	}
// 	ros::spin();
}