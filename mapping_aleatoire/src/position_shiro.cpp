#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <iostream>
#include <boost/graph/graph_concepts.hpp>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <tf/transform_datatypes.h>
#include <boost/thread/mutex.hpp>
ros::Publisher publisher_posInit, publisher_posFinal, publisher_posInit_rviz, publisher_posFinal_rviz;
/*std::mutex mutex_bool;
std::lock_guard<std::mutex> lk(mutex_bool);*/
boost::mutex mutex_bool;

bool b_final, b_init;
geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in, bool& reussi);


void EnvoieDonnee(ros::NodeHandle n);
geometry_msgs::PoseStamped p_init, p_final;
geometry_msgs::PointStamped p_init_rviz, p_final_rviz;

// retrieve the final position thanks to the "publish point" on Rviz

void p_FinalCallback(geometry_msgs::PointStamped data)
{// recuperation repere map sur rviz
	b_final = true;
	p_final_rviz.header.frame_id = data.header.frame_id;
	p_final_rviz.point = data.point;

	p_final.pose.position = data.point;
	p_final.header.frame_id = data.header.frame_id;
	p_final.pose.orientation.w = 1;
	p_final.pose.orientation.x = 0;
	p_final.pose.orientation.y = 0;
	p_final.pose.orientation.z = 0;
	
}

//Retrieve the initial position with the odometry

void p_InitCallback(nav_msgs::Odometry data){
	b_init = true;
	p_init_rviz.header.frame_id = data.header.frame_id;
	p_init_rviz.point = data.pose.pose.position;
	//std::cout << "ici" <<std::endl;
	p_init.header.frame_id = data.header.frame_id;
	//std::cout << p_init.header.frame_id << std::endl;
	p_init.pose = data.pose.pose;
	if(b_final){
		std::cout << "position initiale : " << p_init.pose.position.x << " " << p_init.pose.position.y << std::endl;
		p_init = transfo_map_odom(p_init, b_init);
	
		std::cout << "position initiale converti : " << p_init.pose.position.x << " " << p_init.pose.position.y <<  " bool " << b_init <<std::endl;
		//mutex_bool.lock();
		if(b_init ==true){// Si transformation s'est bien passé
		  ros::Rate loop_rate(100);
		  
		
		  publisher_posInit.publish(p_init);
		 
		  sleep(1);
		
		  
		  publisher_posFinal.publish(p_final);
		  
		  sleep(0.2);
		  publisher_posFinal_rviz.publish(p_final_rviz);
		  sleep(0.2);
		  publisher_posInit_rviz.publish(p_init_rviz);
		  
		  std::cout << "Envoie reussi" << std::endl;
		}
		//mutex_bool.unlock();
		b_final = false;
	}
}

// Convert posestamped from map to odom referential
geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in, bool& reussi){
  //mutex_bool.lock();
  std::cout << "dans la fonction transfo" << std::endl;
	geometry_msgs::PoseStamped p_out;
	//bool reussi = false;
	//do{
	  try{
		  //reussi = true;
		  
		  ros::Time t = ros::Time::now();
		  
		  tf::TransformListener tr;
		  tf::StampedTransform transform;
		  tr.setUsingDedicatedThread(true);
		  //std::cout << "avant" << std::endl;
		  tr.waitForTransform("/map", "/odom", t, ros::Duration(0.5));
		  p_in.header.stamp = ros::Time::now();
		  tr.transformPose("map", p_in, p_out);
	  }catch(tf::TransformException& ex){ 	    
		  ROS_ERROR("Error %s\n",ex.what()); 	  
		  reussi = false;
		  
	  }
	  //mutex_bool.unlock();
	//}while(reussi == false);
	//std::cout << "apres" << std::endl;

	return p_out;
}

/* recuperer position initiale et la finale qui correspond au point sur rviz avec clicked point*/
/* Receive the final position with a clicked point in rviz (publish point) , the initial position is reading in the topic /odom" 
   Then these positions are send to PosFinal and PosInit Topic which are reading by "navigation_map_node" whose goal is to compute a path with RRT algorithm */
int main(int argc, char** argv){
	ros::init(argc, argv, "position_shiro");
	ros::NodeHandle n; // declaration noeud
	publisher_posFinal =  n.advertise<geometry_msgs::PoseStamped>("PosFinal",100);
	publisher_posFinal_rviz =  n.advertise<geometry_msgs::PointStamped>("PosFinal_rviz",10);
	publisher_posInit = n.advertise<geometry_msgs::PoseStamped>("PosInit",100);
	publisher_posInit_rviz = n.advertise<geometry_msgs::PointStamped>("PosInit_rviz",10);
	ros::Subscriber subs_posFinal = n.subscribe("clicked_point",1, p_FinalCallback);
	ros::Subscriber subs_posInit = n.subscribe("odom", 1, p_InitCallback);
	
	ros::Rate loop_rate(100);
	while(ros::ok()){
	  
	  ros::spinOnce();
	  loop_rate.sleep();
	}

}
