#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <iostream>
#include <commande/Twist_ordre.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Twist.h>
#include <time.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <tf/transform_datatypes.h>
//#include <boost/graph/graph_concepts.hpp>
// On controle la vitesse du robot
ros::Publisher move_publisher;
ros::Subscriber sensor_subscriber;
ros::Subscriber sensor_subscriber2;
ros::Subscriber sensor_subscriber3;
ros::Subscriber sensor_subscriber4;
ros::Subscriber sensor_subscriber5;
// Topic pour publier la position intiale et finale de notre robot
ros::Publisher publisher_posInit;
ros::Publisher publisher_posFinal;
ros::Subscriber subs_posInit;
ros::Subscriber subs_posFinal;
geometry_msgs::PoseStamped p_init;
geometry_msgs::PoseStamped p_final;
bool b_init ;
bool b_final ;
//
//geometry_msgs::Twist mouvement;// Vitesse robot
commande::Twist_ordre mouvement;

double PI = 3.14159265359;
ros::Time t;
bool decoincement = false;
int compteur_detection = 0;
bool Laser[8] = {false};
double LaserPortee = 0.55;

double aleatoire(double min, double max){
  srand(time(NULL));
  double num = ( ((double)rand()*(max-min)/RAND_MAX)-min);
  return num;
  
}


geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in){
	ros::Time t = ros::Time(0);
	tf::TransformListener tr;
	tf::StampedTransform transform;
	tr.setUsingDedicatedThread(true);
	tr.waitForTransform("/map", "/odom", t,ros::Duration(0.5));
	geometry_msgs::PoseStamped p_out;
	tr.transformPose("map", p_in, p_out);
	return p_out;
}

void initMove( commande::Twist_ordre& m, bool vitesse_null, double vitesse){
    m.t.angular.x = 0;
    m.t.angular.y = 0;
    m.t.angular.z = 0;
    m.t.linear.y = 0;
    m.t.linear.z = 0;
    if(vitesse_null == false){
      m.t.linear.x = 0.0;
      m.t.angular.z = PI/20;
    }
    else{
      m.t.linear.x = vitesse;
      
    }
}
void Move(commande::Twist_ordre& m){
    m.t.angular.x = 0;
    m.t.angular.y = 0;
    m.t.angular.z = 0;
    m.t.linear.x = 0;
    m.t.linear.y = 0;
    m.t.linear.z = 0;
  bool droite = false, gauche = false;
  for(int i = 0; i < 4 ; i++){
    if(Laser[i] == true){
     droite = true;
     break;
    }
  }
  
  for(int i = 4; i < 8 ; i++){
    if(Laser[i] == true){
     gauche = true;
     break;
    }
  }
  
  if( gauche == false && droite == false){
    m.t.linear.x = 0.2;
  }
  else if(gauche == true && droite == true){
     m.t.angular.z = PI/5;
  }
  else if( gauche ==true && droite == false){
     m.t.angular.z = PI/10; //-PI/10;
  }
  else{
     m.t.angular.z = PI/10;
  }
}

void sensor_callback_simu_1(sensor_msgs::PointCloud data){
  //std::cout << "laser 1 mise à jour" << std::endl;
  for(int i = 1; i <= 6 ; i++){
      float intensity = data.points[i].x;
      if(intensity < LaserPortee){
	Laser[i] = true;
      }else{
	Laser[i] = false;
      }
  
    
  }
}


void init_callback(nav_msgs::Odometry data){
  b_init = true;
  p_init.pose = data.pose.pose;
  p_init.header.frame_id = data.header.frame_id;
  p_init = transfo_map_odom(p_init);
}

void final_callback(nav_msgs::Odometry data){
  b_final = true;
  p_final.pose = data.pose.pose;
  p_final.header.frame_id = data.header.frame_id;
  p_final = transfo_map_odom(p_final);
}

void subscriber_pose_init(ros::NodeHandle n){ // Odometry
  b_init = false;
  subs_posInit = n.subscribe("odom",100, &init_callback);
  ros::Rate r(10);
  //cout << "avant while " << BoolMap << endl;
  while(n.ok() && b_init == false){
   // cout << " dans la boucle" << endl;
    ros::spinOnce();
    //r.sleep();
  }
 subs_posInit.shutdown();
 std::cout << "position intiale recup" << std::endl;
}

void subscriber_pose_final(ros::NodeHandle n){
  b_final = false;
  //subs_posFinal = n.subscribe("odom",100, &final_callback);
  subs_posFinal = n.subscribe("odom",100, &final_callback);
  ros::Rate r(10);
  //cout << "avant while " << BoolMap << endl;
  while(n.ok() && b_final == false){
   // cout << " dans la boucle" << endl;
    ros::spinOnce();
    //r.sleep();
  }
  subs_posFinal.shutdown();
  std::cout << "position final recupéré" << std::endl;
}


int main(int argc, char** argv){
	ros::init(argc, argv, "mapping_aleatoire_real_shiro");
	ros::NodeHandle n; // declaration noeud
	ros::Time t, t0, now;
	bool time = false;
	t = ros::Time::now();
	mouvement.active = true;
	subscriber_pose_init(n);

	std::cout << " X Final = " << p_init.pose.position.x << " Y Final = " << p_init.pose.position.y << std::endl;


	
	// Declaration publisher / Subscriber pour les lasers
	move_publisher = n.advertise<commande::Twist_ordre>("/cmd_vel_ordre_0",10);// creation d'un publisher pour le topic cmd_vel
	sensor_subscriber = n.subscribe<sensor_msgs::PointCloud>("/RosAria/sonar", 1000, sensor_callback_simu_1);// bumper_state



	
	ros::Rate loop_rate(100);
	// Initialisation du mouvement
	initMove(mouvement, true,0.2);
	int count = 0;
	t0 = ros::Time::now();
	while(ros::ok() ){
	  //std::cout <<ros::Time::now()-t0 << std::endl;
	    now = ros::Time::now();
	    move_publisher.publish(mouvement);
	    
	    if(now-t0 > ros::Duration(6.0)){
	      break;
	    }
	    
	    ros::spinOnce();
	    loop_rate.sleep();
	  
	  
	}
	
	
	std::cout << " Initialisation vitesse réalisé " << std::endl;
	// rotation robot

	initMove(mouvement, false,0.0);
	t0 = ros::Time::now();
	while(ros::ok() ){
	  //std::cout <<ros::Time::now()-t0 << std::endl;
	    now = ros::Time::now();
	    move_publisher.publish(mouvement);
	    
	    if(now-t0 > ros::Duration(42.0)){
	      break;
	    }
	    
	    ros::spinOnce();
	    loop_rate.sleep();
	  
	  
	}
	// Deplacement aléatoire du robot
	t0 = ros::Time::now();
	while(ros::ok()){
	  //std::cout <<ros::Time::now()-t0 << std::endl;
	      now = ros::Time::now();
	      
	    if(now-t0 > ros::Duration(100.0)){
	      break;
	    }
	    
	    if( now - t > ros::Duration(0.5)){
	      Move(mouvement);
	      move_publisher.publish(mouvement);
	      time = true;
	      
	      //std:: cout << " Mise à jour vitesse " << std::endl;
	    }
	    else{
	      time = false; 
	    }
	    
	    ros::spinOnce();
	    loop_rate.sleep();
	    
	    if(time == true){
		t = ros::Time::now();
	    }
	  
	}
	initMove(mouvement, true,0.0);
	move_publisher.publish(mouvement);
	subscriber_pose_final(n);

	std::cout << " X Init = " << p_final.pose.position.x << " Y Init = " << p_final.pose.position.y << std::endl;
	
	/* Publication des pos intiales et finales */
	publisher_posFinal =  n.advertise<geometry_msgs::PoseStamped>("PosInit",10);
	while(publisher_posFinal.getNumSubscribers() == 0){
	    
	    loop_rate.sleep();
	
	}
	publisher_posFinal.publish(p_final);
	ros::spinOnce();
	
	publisher_posInit = n.advertise<geometry_msgs::PoseStamped>("PosFinal",10);

	
	while(publisher_posInit.getNumSubscribers() == 0){
      
	  loop_rate.sleep();
   
	}
	publisher_posInit.publish(p_init);
	ros::spinOnce();


	
	return 0;
}
