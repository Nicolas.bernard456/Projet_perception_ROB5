#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <iostream>
#include <boost/graph/graph_concepts.hpp>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <tf/transform_datatypes.h>
ros::Publisher publisher_posInit;
ros::Publisher publisher_posFinal;
ros::Publisher publisher_posInit_rviz;
ros::Publisher publisher_posFinal_rviz;
ros::Subscriber subs_posInit;
ros::Subscriber subs_posFinal;

bool b_final, b_init;
void p_FinalCallback(geometry_msgs::PointStamped data);
void p_Final(ros::NodeHandle n);
void p_Init(ros::NodeHandle n);
void p_InitCallback(nav_msgs::Odometry data);
void EnvoieDonnee2(ros::NodeHandle n);
geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in);

void EnvoieDonnee(ros::NodeHandle n);
geometry_msgs::PoseStamped p_init;
geometry_msgs::PoseStamped p_final;
geometry_msgs::PointStamped p_init_rviz;
geometry_msgs::PointStamped p_final_rviz;

/* recuperer position initiale et la finale qui correspond au point sur rviz avec clicked point*/
int main(int argc, char** argv){
	ros::init(argc, argv, "position_shiro");
	ros::NodeHandle n; // declaration noeud
	
	ros::Rate r(100);
	while(ros::ok()){
	  p_Final(n);
	  EnvoieDonnee(n);
	  p_Init(n);
	  EnvoieDonnee2(n);
	 r.sleep(); 
	}
}


void EnvoieDonnee(ros::NodeHandle n){
	ros::Rate loop_rate(100);
	

	publisher_posFinal =  n.advertise<geometry_msgs::PoseStamped>("PosFinal",10);
	while(publisher_posFinal.getNumSubscribers() == 0){
	    
	    loop_rate.sleep();
	
	}
	publisher_posFinal.publish(p_final);
	//ros::spinOnce();
  
	publisher_posFinal_rviz =  n.advertise<geometry_msgs::PointStamped>("PosFinal_rviz",1);
	while(publisher_posFinal_rviz.getNumSubscribers() == 0){
	    
	    loop_rate.sleep();
	
	}
	publisher_posFinal_rviz.publish(p_final_rviz);
	//ros::spinOnce();
	std::cout << "message envoyé" << std::endl;
}
void EnvoieDonnee2(ros::NodeHandle n){
	ros::Rate loop_rate(100);
	 
	  publisher_posInit = n.advertise<geometry_msgs::PoseStamped>("PosInit",10);
	
	while(publisher_posInit.getNumSubscribers() == 0){
      
	  loop_rate.sleep();
   
	}
	publisher_posInit.publish(p_init);
	//ros::spinOnce();

	publisher_posInit_rviz = n.advertise<geometry_msgs::PointStamped>("PosInit_rviz",1);
	
	while(publisher_posInit_rviz.getNumSubscribers() == 0){
      
	  loop_rate.sleep();
   
	}
	publisher_posInit_rviz.publish(p_init_rviz);
	//ros::spinOnce();

	
	std::cout << "message envoyé" << std::endl;
}
void p_Final(ros::NodeHandle n){
  b_final = false;
  ros::Rate r(10);
  subs_posFinal = n.subscribe("clicked_point",1, p_FinalCallback);
  //cout << "avant while " << BoolMap << endl;
  while(n.ok() && b_final == false){
 
    ros::spinOnce();
    r.sleep();
  }
  //subs_posFinal.shutdown();
  std::cout << "point final trouvé"<<std::endl;
}

void p_Init(ros::NodeHandle n){// A convertir dans repere map
  b_init = false;
  subs_posInit = n.subscribe("odom",1, p_InitCallback);
  ros::Rate r(10);
  //cout << "avant while " << BoolMap << endl;
  while(n.ok() && b_init == false){
   // cout << " dans la boucle" << endl;
    ros::spinOnce();
    r.sleep();
  }
 //subs_posInit.shutdown();
 std::cout << "point initiale trouve"<<std::endl;
}

void p_FinalCallback(geometry_msgs::PointStamped data)
{// recuperation repere map sur rviz
  
  b_final = true;
  p_final_rviz.header.frame_id = data.header.frame_id;
  p_final_rviz.point = data.point;
  
  p_final.pose.position = data.point;
  p_final.header.frame_id = data.header.frame_id;
  p_final.pose.orientation.w = 1;
  p_final.pose.orientation.x = 0;
  p_final.pose.orientation.y = 0;
  p_final.pose.orientation.z = 0;
}

void p_InitCallback(nav_msgs::Odometry data){
  b_init = true;
  p_init_rviz.header.frame_id = data.header.frame_id;
  p_init_rviz.point = data.pose.pose.position;
  //std::cout << "ici" <<std::endl;
  p_init.header.frame_id = data.header.frame_id;
  //std::cout << p_init.header.frame_id << std::endl;
  p_init.pose = data.pose.pose;
  p_init = transfo_map_odom(p_init);
}
geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in){
	  geometry_msgs::PoseStamped p_out;
	try{
	ros::Time t = ros::Time::now();
	tf::TransformListener tr;
	tf::StampedTransform transform;
	tr.setUsingDedicatedThread(true);
	std::cout << "avant" << std::endl;
	tr.waitForTransform("/map", "/odom", t,ros::Duration(3.0));
	  
	
	
	
	tr.transformPose("map", p_in, p_out);
	}catch(tf::TransformException& ex){ 	    
	  ROS_ERROR("Error %s",ex.what()); 	  
	  
	}
	std::cout << "apres" << std::endl;
	return p_out;
}