#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <iostream>
#include <commande/Twist_ordre.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <tf/transform_datatypes.h>

#include <time.h>
#include <boost/graph/graph_concepts.hpp>
//#define TIME_ROTATE 21.0
//#define TIME_RANDOM 50.0
// On controle la vitesse du robot = to control the velocity of the robot
ros::Publisher move_publisher;

// To determine the value of the sensors
ros::Subscriber sensor_subscriber;
ros::Subscriber sensor_subscriber2;
ros::Subscriber sensor_subscriber3;
ros::Subscriber sensor_subscriber4;
ros::Subscriber sensor_subscriber5;
// Topic pour publier la position intiale et finale de notre robot = topic to publish and subscribe the initial and final position of the robot

ros::Publisher publisher_posInit;
ros::Publisher publisher_posFinal;
ros::Subscriber subs_posInit;
ros::Subscriber subs_posFinal;
geometry_msgs::PoseStamped p_init;
geometry_msgs::PoseStamped p_final;
// Variable
bool b_init ;
bool b_final ;
commande::Twist_ordre mouvement;
double PI = 3.14159265359;
bool Laser[5] = {false};
double LaserPortee = 0.6;// Scope of the laser

double TIME_ROTATE;
double TIME_RANDOM;

/* Functions */

double aleatoire(double min, double max);
geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in);
void initMove( commande::Twist_ordre& m, bool vitesse_null, double vitesse);
void Move(commande::Twist_ordre& m);
void sensor_callback_simu_1(sensor_msgs::LaserScan data);
void sensor_callback_simu_2(sensor_msgs::LaserScan data);
void sensor_callback_simu_3(sensor_msgs::LaserScan data);
void sensor_callback_simu_4(sensor_msgs::LaserScan data);
void sensor_callback_simu_5(sensor_msgs::LaserScan data);
void init_callback(nav_msgs::Odometry data);
void final_callback(nav_msgs::Odometry data);
void subscriber_pose_init(ros::NodeHandle n);
void subscriber_pose_final(ros::NodeHandle n);
void checking_environnement(commande::Twist_ordre& m);
void movement_random(bool time, commande::Twist_ordre& m, ros::Time& t);

/* The node permits to explore the environment randomly and then after a while the robot will return to its initial position using the node navigation_map_node which use RRT algorithm */
/* topic where the node publish the differents positions : "PosInit" and "PosFinal" which are read by navigation_map_node */
int main(int argc, char** argv){
	bool check = true;
	TIME_RANDOM = 50.0;
	TIME_ROTATE = 21.0;
	
	// If their are argument during the call of this node
	if(argc > 1){
		if(strcmp(argv[1],"false")==0){
			if(argc == 3){
				check = false;
				TIME_RANDOM = atof(argv[2]);
				TIME_ROTATE = 0.0;
			}
			else{
				std::cout << " Argument error, retry ! " << std::endl;
				return 0;
			}	
		}
		
		else if(strcmp(argv[1],"true")==0){
			if(argc == 4){
				check = true;
				TIME_RANDOM = atof(argv[2]);
				TIME_ROTATE = atof(argv[3]);
			}
			else{
				std::cout << " Argument error, retry ! " << std::endl;
				return 0;
			}
			
			
		}
	}
	
	ros::init(argc, argv, "mapping_aleatoire");
	ros::NodeHandle n; // declaration noeud
	ros::Time t;
	bool time = false;
	t = ros::Time::now();
	ros::Rate loop_rate(100);
	mouvement.active = true;
	
	// To determine the initial position at the beggining
	subscriber_pose_init(n);
	std::cout << " X final = " << p_init.pose.position.x << " Y final = " << p_init.pose.position.y << std::endl;
	
	// publisher / Subscriber for lasers and topic velocity
	move_publisher = n.advertise<commande::Twist_ordre>("/cmd_vel_ordre_0",10);// creation d'un publisher pour le topic cmd_vel
	sensor_subscriber = n.subscribe<sensor_msgs::LaserScan>("/IR1", 1000, sensor_callback_simu_1);// bumper_state
	sensor_subscriber2 = n.subscribe<sensor_msgs::LaserScan>("/IR2", 1000, sensor_callback_simu_2);// bumper_state
	sensor_subscriber3 = n.subscribe<sensor_msgs::LaserScan>("/IR3", 1000, sensor_callback_simu_3);// bumper_state
	sensor_subscriber4 = n.subscribe<sensor_msgs::LaserScan>("/IR4", 1000, sensor_callback_simu_4);// bumper_state
	sensor_subscriber5 = n.subscribe<sensor_msgs::LaserScan>("/IR5", 1000, sensor_callback_simu_5);// bumper_state
	
	// Initialisation of the different movement
	std::cout << " TIME_ROTATE = " << TIME_ROTATE << " TIME_RANDOM = " << TIME_RANDOM << std::endl;
	// The robot will move forward during one second then rotate during TIME_ROTATE so as to check the environment
	if(check) 
		checking_environnement(mouvement);
	
	// The robot will move randomly avoiding the obstacle during TIME_RANDOM
	
	movement_random(time, mouvement, t);
	
	
	// To determine the final position of the robot
	subscriber_pose_final(n);
	std::cout << " X final = " << p_final.pose.position.x << " Y final = " << p_final.pose.position.y << std::endl;
	
	
	/* Publish Initial and final pos which will be read by the node : navigation_map_node */
	
	publisher_posInit = n.advertise<geometry_msgs::PoseStamped>("PosFinal",10);
	
	publisher_posFinal =  n.advertise<geometry_msgs::PoseStamped>("PosInit",10);
	
	while(publisher_posFinal.getNumSubscribers() == 0){
		
		loop_rate.sleep();
		
	}
	publisher_posFinal.publish(p_final);
	
	while(publisher_posInit.getNumSubscribers() == 0){
		
		loop_rate.sleep();
		
	}
	publisher_posInit.publish(p_init);
	
	ros::spinOnce();
	
	return 0;
}


double aleatoire(double min, double max){
	srand(time(NULL));
	double num = ( ((double)rand()*(max-min)/RAND_MAX)-min);
	return num;
	
}


//Fonction transformant une position d'un repere vers un autre
// convert position from map to odom
geometry_msgs::PoseStamped transfo_map_odom(geometry_msgs::PoseStamped p_in){
	ros::Time t = ros::Time::now();

	tf::TransformListener tr;
	tf::StampedTransform transform;
	tr.setUsingDedicatedThread(true);
	tr.waitForTransform("/map", "/odom", t,ros::Duration(0.5));
	geometry_msgs::PoseStamped p_out;
	tr.transformPose("map", p_in, p_out);
	return p_out;
}

// Change the value of the commande (velocity of the robot) depending of the argument of the function 
void initMove( commande::Twist_ordre& m, bool vitesse_null, double vitesse){
	m.t.angular.x = 0;
	m.t.angular.y = 0;
	m.t.angular.z = 0;
	m.t.linear.y = 0;
	m.t.linear.z = 0;
	if(vitesse_null == false){
		m.t.linear.x = 0.0;
		m.t.angular.z = PI/10;
	}
	else{
		m.t.linear.x = vitesse;
		
	}
}

// When the laser 
void Move(commande::Twist_ordre& m){
	m.t.angular.x = 0;
	m.t.angular.y = 0;
	m.t.angular.z = 0;
	m.t.linear.x = 0;
	m.t.linear.y = 0;
	m.t.linear.z = 0;
	std::cout << Laser[0] << " " << Laser[1] << " " << Laser[2] << " " << Laser[3] << " " << Laser[4] << std::endl;
	if(Laser[0]==false && Laser[1] == false && Laser[2] == false && Laser[3] == false && Laser[4] == false){
		m.t.linear.x = 0.5;
	}
	else if(Laser[0]==true && Laser[1] == true && Laser[2] == true && Laser[3] == true && Laser[4] == true){
		m.t.angular.z = PI/5;
		
	}
	else if( (Laser[0] == true || Laser[1] ==true) && (Laser[3] == false && Laser[4] == false)){
		m.t.angular.z = -PI/10;
		//m.angular.z = -aleatoire(0.0,PI)/2;
	}
	else if( (Laser[3] == true || Laser[4] ==true) && (Laser[1] == false && Laser[2] == false)){
		m.t.angular.z = PI/10;
		//m.angular.z = aleatoire(0.0,PI)/2;
	}else{
		m.t.angular.z = PI/5; 
	}
}

/* Check the value of the 5 lasers */
void sensor_callback_simu_1(sensor_msgs::LaserScan data){
	//std::cout << "laser 1 mise à jour" << std::endl;
	float intensity = data.ranges[0];
	if(intensity < LaserPortee- 0.01){
		Laser[0] = true;
	}else{
		Laser[0] = false;
	}
}

void sensor_callback_simu_2(sensor_msgs::LaserScan data){
	float intensity = data.ranges[0];
	if(intensity < LaserPortee- 0.01){
		Laser[1] = true;
	}else{
		Laser[1] = false;
	}
}

void sensor_callback_simu_3(sensor_msgs::LaserScan data){
	//std::cout << "laser 3 mise à jour" << std::endl;
	float intensity = data.ranges[0];
	if(intensity < LaserPortee- 0.01){
		Laser[2] = true;
	}else{
		Laser[2] = false;
	}
}

void sensor_callback_simu_4(sensor_msgs::LaserScan data){
	float intensity = data.ranges[0];
	if(intensity < LaserPortee- 0.01){
		Laser[3] = true;
	}else{
		Laser[3] = false;
	}
}

void sensor_callback_simu_5(sensor_msgs::LaserScan data){
	float intensity = data.ranges[0];
	if(intensity < LaserPortee- 0.01){
		Laser[4] = true;
	}else{
		Laser[4] = false;
	}
}

//Determine the coordinate of the initial position ( which will be the position of the return of base  so the final position in the RRT algorithm)
void init_callback(nav_msgs::Odometry data){// convert odom to map
	b_init = true;
	p_init.pose = data.pose.pose;
	p_init.header.frame_id = data.header.frame_id;
	p_init = transfo_map_odom(p_init);
}

//Determine the coordinate of the final position ( which will be the initial position in the RRT algorithm)
void final_callback(nav_msgs::Odometry data){// convert odom to map
	b_final = true;
	
	p_final.pose = data.pose.pose;
	p_final.header.frame_id = data.header.frame_id;
	p_final = transfo_map_odom(p_final);
}

// Determine the initial position
void subscriber_pose_init(ros::NodeHandle n){
	b_init = false;
	subs_posInit = n.subscribe("odom",100, &init_callback);
	ros::Rate r(10);
	//cout << "avant while " << BoolMap << endl;
	while(n.ok() && b_init == false){
		// cout << " dans la boucle" << endl;
		ros::spinOnce();
		//r.sleep();
	}
	subs_posInit.shutdown();
	std::cout << "position intiale recup" << std::endl;
}

//Determine the final position
void subscriber_pose_final(ros::NodeHandle n){
	b_final = false;
	subs_posFinal = n.subscribe("odom",100, &final_callback);
	ros::Rate r(10);
	//cout << "avant while " << BoolMap << endl;
	while(n.ok() && b_final == false){
		// cout << " dans la boucle" << endl;
		ros::spinOnce();
		//r.sleep();
	}
	subs_posFinal.shutdown();
	std::cout << "position final recupéré" << std::endl;
}

// Checking the environment before randomly movement
void checking_environnement(commande::Twist_ordre& m){
	ros::Rate loop_rate(100);
	ros::Time t0, now;
	
	// change the velocity to 0.8
	initMove(m, true,0.8);
	t0 = ros::Time::now();
	while(ros::ok() ){
		now = ros::Time::now();
		move_publisher.publish(m);
		
		if(now-t0 > ros::Duration(1.0)){
			break;
		}
		
		ros::spinOnce();
		loop_rate.sleep();
		
		

	}
	
	
	std::cout << " Initialisation vitesse réalisé " << std::endl;
	// rotation robot
	
	initMove(m, false,0.0);
	t0 = ros::Time::now();
	
	while(ros::ok() ){
		//std::cout <<ros::Time::now()-t0 << std::endl;
		now = ros::Time::now();
		move_publisher.publish(m);
		
		if(now-t0 > ros::Duration(TIME_ROTATE)){
			break;
		}
		
		ros::spinOnce();
		loop_rate.sleep();
		
		
	}
}

// Explore randomly the environment
void movement_random(bool time, commande::Twist_ordre& m, ros::Time& t){
	ros::Rate loop_rate(100);
	ros::Time t0, now;
	t0 = ros::Time::now();
	while(ros::ok()){
		//std::cout <<ros::Time::now()-t0 << std::endl;
		now = ros::Time::now();
		
		if(now-t0 > ros::Duration(TIME_RANDOM)){
			break;
		}
		
		if( now - t > ros::Duration(0.5)){
			Move(m);
			move_publisher.publish(m);
			time = true;
			
			//std:: cout << " Mise à jour vitesse " << std::endl;
		}
		else{
			time = false; 
		}
		
		ros::spinOnce();
		loop_rate.sleep();
		
		if(time == true){
			t = ros::Time::now();
		}
		
	}
	initMove(mouvement, true,0.0);
	move_publisher.publish(m);
	
}



