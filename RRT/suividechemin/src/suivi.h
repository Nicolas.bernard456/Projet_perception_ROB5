#ifndef SUIVI_H
#define SUIVI_H
#include <iostream>
#include <vector>
#include <string>
#include <boost/graph/graph_concepts.hpp>
#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Joy.h"
#include <../../opt/ros/indigo/include/geometry_msgs/Twist.h>
#include "nav_msgs/Odometry.h"
#define PI 3.1415926535

class Suivi{
public :
  //void CalculCommande(int indice);
  //void envoie_commande();
  //void SubscriberPose(const nav_msgs::Odometry::ConstPtr& msg);
  //double distance_proche();
  //void addChemin(std::vector< std::vector<int> > G);
  Suivi();
  //void Suivi_chemin();

private:
  double u1;
  double u2;
  double l1;
  double K;
  double theta_e;
  double distance;
  geometry_msgs::Twist commande_vitesse;
  geometry_msgs::Pose Pose;
  ros::Publisher move_publisher;
  bool chemin_fini;
  std::vector< std::vector<int> > chemin;
  
  
};

#endif