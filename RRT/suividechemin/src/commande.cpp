#include "ros/ros.h"
#include <iostream>
#include <vector>
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Pose.h"
//#include "/home/shiro/catkin_ws/src/perception/Projet_perception_ROB5/RRT/navigation_map/src/navigation_map.h"
//extern bool Chemin_lu ;
using namespace std;


ros::Publisher publisher_chemin;
ros::Subscriber subs;
vector<geometry_msgs::Point> chemin;
bool boolChemin = false; // chemin chargé

void cheminCallback(geometry_msgs::PoseArray data){
  cout << "in function" << endl;

  geometry_msgs::PoseArray msg = data;
 
  boolChemin = true;

 for(int i =0;i < msg.poses.size() ; i++){
    geometry_msgs::Point p = msg.poses[i].position;
    chemin.push_back(p);
 }

  
}

void Affichage_points(vector<geometry_msgs::Point> data){
 
  int taille =  data.size();
 cout << " AFFICHAGE POINTS " << endl;
 
 for(int i=0; i< taille; i++){
  cout << "X = " << data[i].x << " Y = " << data[i].y << endl; 
 
 }

}


int main(int argc, char** argv){
  ros::init(argc, argv, "suividechemin");
  ros::NodeHandle n; // declaration noeud
  ros::Rate r(10);
  subs = n.subscribe("/Chemin", 1, cheminCallback);
  while( boolChemin == false){
    ros::spinOnce();
    //r.sleep();
  }
  Affichage_points(chemin);
  
  return 0;
  
}