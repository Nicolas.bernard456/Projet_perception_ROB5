#ifndef GRAPH_H
#define GRAPH_H
#include "ros/ros.h"
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "arbre.h"
//#include <array>
#include <opencv2/opencv.hpp>
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Pose.h"



long Factoriel(long n);
long Combinaison(long n, long k);
float dist_pose(geometry_msgs::PoseStamped p1 , geometry_msgs::PoseStamped p2);
std::vector< geometry_msgs::PoseStamped > add_point(std::vector< geometry_msgs::PoseStamped > p);

/* Graph Class */

class Graph{
public:
	nav_msgs::Path ConvertChemin(std::vector< std::vector<int> > chemin, int origineX, int origineY);
	
	// Constructor and destructor
	Graph();
	~Graph();
	
	// Getters
	std::vector<int> getPoint_x();
	std::vector<int> getPoint_y();
	int getX_origin();
	int getY_origin();
	double getResolution();
	int getTailleX();
	int getTailleY();
	std::vector< std::vector<int> > getChemin();
	std::vector< std::vector<int> > getCheminLisse();
	
	// setters
	void setPoint_x(int ValueToAdd);
	void setPoint_y(int ValueToAdd);
	void setPoint(int x,int y);
	
	// Map
	bool CreateImage();
	void loadMap();
	void mapCallback(nav_msgs::OccupancyGrid msg);
	int ValueNavMap(int x, int y );
	void ChangeValueNavMap(int x, int y, int value );
	void Filtrage();
	
	// Display
	void Affichage(std::vector< std::vector<int> > v);
	void displayImage();
	void DisplayChemin(std::vector< std::vector <int> > graph, std::string name );
	void DrawOnlyCircle(cv::Mat img, std::vector<int> qpoint, std::string nameWindow, int wait,cv::Scalar color, int diametre);
	void DrawCircleLine(cv::Mat img, std::vector<int> qpoint, std::vector<int> qnear, std::string nameWindow, int wait,cv::Scalar color1,  int diametre);
	
	// RRT Algorithm
	
		void test();
	bool RRT_Connect(std::vector<int> qinit, std::vector<int> qfinal , int deltaq, int increment, int pixel);
	bool RRT_Connectv2(std::vector<int> qinit, std::vector<int> qfinal , int deltaq, int increment, int pixel);
	
	void RRT_qrand(std::vector<int>& tab,int t_X, int t_Y);
	bool RRT_extend(std::vector< std::vector<int> > Graph, std::vector<int>& qrand, int deltaq, std::vector<int>& qnewPoint, std::vector<int>& qnear, int pixel);
	void RRT_qnear(std::vector<int>& qnear, int& distance, std::vector<int> qrand,  std::vector< std::vector<int> > Graph);
	int getDistance(int x1, int y1, int x2, int y2);
	bool RTT_IsArrived(std::vector<int> q1, std::vector<int> q2, int deltaq, int pixel);
	bool IsObstacleSansPortee(int x, int y);
	bool IsObstacleAvecPortee(int x, int y, int pixel);
		
	void GraphBezier();
	
private:
	std::vector<int> point_x;
	std::vector<int> point_y;
	int tailleX,tailleY;
	double resolution;
	ros::Subscriber listener;
	ros::NodeHandle n;
	nav_msgs::OccupancyGrid map;
	bool BoolMap;
	/* Variable pour RRT */
	cv::Mat Carte;
	std::vector< std::vector<int> > G;
	std::vector< std::vector<int> > Glisse;
	//std::vector< std::vector<int> > Gf;
	int x_origin;
	int y_origin;

};

#endif