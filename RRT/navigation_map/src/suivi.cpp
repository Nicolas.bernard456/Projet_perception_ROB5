#include "suivi.h"


// convertisseur
double degree2radian(double angle){
    return 3.141592*angle/180.0;
}

//Class
Suivi::Suivi(){
 u1 = 1.0; 
 l1 = 0.5;
 theta_e = 0.0;
 chemin_fini = false;
 K = 1;
 
 // Initialisation
 commande_vitesse.linear.x = 0;
 commande_vitesse.linear.y = 0;
 commande_vitesse.linear.z = 0;
 commande_vitesse.angular.x = 0;
 commande_vitesse.angular.y = 0;
 commande_vitesse.angular.z = 0;
}

/*void Suivi::addChemin(std::vector< std::vector<int> > G){
    chemin = G;
}*/



double Suivi::distance_proche(){
      double indice = 0 ;
      int size = chemin.size();
      double temp;
      distance = sqrt( pow( (chemin[0].at(0)-Pose.position.x),2) + pow( (chemin[0].at(1)-Pose.position.y),2) ) ;
      
      for(int i = 1; i < size ; i++){
	temp = sqrt( pow( (chemin[i].at(0)-Pose.position.x),2) + pow( (chemin[i].at(1)-Pose.position.y),2) );
	if(distance > temp){
	  distance = temp;
	  indice = i;
	}
      }
      return indice;
 }
 
 
void Suivi::CalculCommande(int indice){
  // Calcul xs , yS
  vector <double> ys, xs; // Vecteur ys non unitaire
  double norm_ys;
  ys.push_back(Pose.position.x - chemin[i].at(0));
  ys.push_back(Pose.position.y - chemin[i].at(1));
  norm_ys = sqrt(pow( ys.at(0), 2) + pow( ys.at(1), 2) );
  ys.at(0) = ys.at(0)/norm_ys;
  ys.at(1) = ys.at(1)/norm_ys;
  xs.push_back( ys.at(1) * ys.at(1) / ( ys.at(1) + ys.at(0)* ys.at(0) ) ;
  xs.push_back( ( xs.at(0) - ys.at(1) ) / ys.at(0) );
  
  // Calcul theta : quaternion to radian
  tf::Quaternion q(Pose.orientation.x, Pose.orientation.y, Pose.orientation.z, Pose.orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  
  // Commande
  double theta_s = atan2(xs.at(1),xs.at(0));
  double theta = yaw;
  theta_e = theta - theta_s;
  std::cout << " theta_e = " << theta_e << std::endl;
  u2 = -u1*sin(theta_e) /(l1 * cos(theta_e)) - u1 * K * distance / (cos(theta_e));
  commande_vitesse.linear.x = u1;
  commande_vitesse.angular.z = u2;
}
void Suivi::envoie_commande(){
  move_publisher.publish(commande_vitesse);
  
  
}

/*void Suivi::Suivi_chemin(){
  ros::NodeHandle n; // declaration noeud
  this.move_publisher = n.advertise<geometry_msgs::Twist>("cmd_vel/",10);//declaration publisher vers topic de la simu gazebo
  ros::Subscriber sub = n.subscribe("odom", 1000, SubscriberPose);
  ros::Rate loop_rate(1000);
  double indice;
  while(ros::ok() && chemin_fini == false){
    indice = distance_proche();
    CalculCommande(indice);
    envoie_commande();
    chemin.erase(indice);
   
   ros::spinOnce();
   loop_rate.sleep();
  }
  
}
void Suivi::SubscriberPose(const nav_msgs::Odometry::ConstPtr& msg){
  this.Pose = msg->pose.pose.position;
  std::cout << ("Position enrengistrer" << std::endl;
}*/
