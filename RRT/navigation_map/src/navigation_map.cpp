#include "ros/ros.h"
#include "graph.h"
#include "arbre.h"
#include "suivi.h"
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PointStamped.h>
//#include "navigation_map.h"
using namespace std;
ros::Publisher publisher;
bool Chemin_lu ;

/* Recuperation des positions initiales et finales*/
geometry_msgs::PoseStamped pose_init;
geometry_msgs::PoseStamped pose_final;
ros::Subscriber subs_posInit;
ros::Subscriber subs_posFinal;

bool init, final;
void init_callback(geometry_msgs::PoseStamped data);
void final_callback(geometry_msgs::PoseStamped data);
void subscriber_pose_init(ros::NodeHandle n);
void subscriber_pose_final(ros::NodeHandle n);
void EnvoieDonneeCommande( ros::NodeHandle n, nav_msgs::Path chemin);


int main(int argc, char** argv){
	
	ros::init(argc, argv, "navigation_map");
	ros::NodeHandle n; // declaration noeud
	std::vector<int> qi;
	std::vector<int> qf;
	// Receive the initial position and final position
	subscriber_pose_init(n);
	subscriber_pose_final(n);
	
	
	
	
	Graph g;
	// Loading the map and creation
	g.loadMap();
	if(g.CreateImage() == false){
		cout << "creation of the image impossible\n" << endl;
		return 0;
	}
	// Filter the map 
	g.Filtrage();
	
	
	
	// save the initial point et final point in the global reference
	cout << "Creation of the image sucess ! " << endl;
	qi.push_back(round( (pose_init.pose.position.x -g.getX_origin() )/g.getResolution() ));
	qi.push_back(round( g.getTailleY() - ( pose_init.pose.position.y -g.getY_origin())/g.getResolution() ));
	qf.push_back(round( (pose_final.pose.position.x -g.getX_origin() )/g.getResolution() ) );
	qf.push_back(round(g.getTailleY()- (pose_final.pose.position.y-g.getY_origin())/g.getResolution() ));
	
	// Repere map
	/*cout << "Position Final : " << pose_final.pose.position.x << " " << pose_final.pose.position.y << endl;
	 *  cout << "Position Initiale : " << pose_init.pose.position.x << " " << pose_init.pose.position.y << endl;
	 *  cout << "Position Final : " << qi.at(0) << " " << qi.at(1) << endl;
	 *  cout << "Position Initiale :" << qf.at(0) << " " << qf.at(1) << endl;*/
	
	
	//g.displayImage();
	int pas = 90;
	
	if(argc == 2){
		pas = atoi(argv[1]);
	}
	
	
	cout << " RRT Bidirectionnal Resolution : " << pas << endl;
	if(g.RRT_Connectv2(qi, qf, pas, 10000, 0)==false){
		return 0;
	}
	//g.DisplayChemin(g.getChemin(), "chemin");
	g.GraphBezier();
	//g.Affichage(g.getCheminLisse());
	g.DisplayChemin(g.getCheminLisse(),"cheminL");
	
	/* Conversion du chemin pour l'envoyer au topic "Chemin_t" */
	/* Convert the path in a type in order to send it to the topic "Chemin_t" */
	cout << "Convert ..." << endl;
	nav_msgs::Path chemin = g.ConvertChemin(g.getCheminLisse(), g.getX_origin(), g.getY_origin());
	cout << "Envoie de donnée en cours ... " << endl;
	EnvoieDonneeCommande(n, chemin);
	
	return 0; 

}


/************/
/* Fonction */
/************/


// Send the path to the topic "Chemin_t" which will be read by the node "update_path_shiro"
void EnvoieDonneeCommande( ros::NodeHandle n, nav_msgs::Path chemin){
	publisher = n.advertise<nav_msgs::Path>("Chemin_t",100);//declaration publisher vers 
	ros::Rate r(100);
	
	while(publisher.getNumSubscribers() == 0){
		
		r.sleep();
		
	}
	publisher.publish(chemin);
	ros::spinOnce();
	publisher.publish(chemin);
	cout << " Path received by the command " << Chemin_lu<< endl;
}   




void init_callback(geometry_msgs::PoseStamped data){
	init = true;
	pose_init = data;
	cout <<"init recu"<<endl;
	
}

void final_callback(geometry_msgs::PoseStamped data){
	final = true;
	pose_final = data;
	cout <<"final recu"<<endl;
}


// Subscribe the initial position
void subscriber_pose_init(ros::NodeHandle n){
	init = false;
	subs_posInit = n.subscribe("PosInit", 10, &init_callback);
	ros::Rate r(100);
	//cout << "avant while " << BoolMap << endl;
	while(ros::ok() && init == false){
		
		ros::spinOnce();
		r.sleep();
	}
	subs_posInit.shutdown();
}

// Subscribe the final position
void subscriber_pose_final(ros::NodeHandle n){
	cout << "debut pose final fonction " << endl;
	final = false;
	subs_posFinal = n.subscribe("PosFinal", 10, &final_callback);
	ros::Rate r(100);
	//cout << "avant while " << BoolMap << endl;
	while(ros::ok() && final == false){
		//cout << " rosok final "<< endl;
		// cout << " dans la boucle" << endl;
		ros::spinOnce();
		r.sleep();
	}
	subs_posFinal.shutdown();
}
