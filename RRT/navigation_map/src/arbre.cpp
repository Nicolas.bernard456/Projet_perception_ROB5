#include "arbre.h"
#include <boost/concept_check.hpp>
#include <algorithm>
bool conditionArret = false;
using namespace std;
Arbre::Arbre(){
  
}
Arbre::Arbre(vector<int> racine){
  this->Pos = racine;
  
}

// Compare if two nodes are the same
bool Arbre::CompareNoeud(vector<int> noeud1, vector<int> noeud2){
  if(noeud1.at(0) == noeud2.at(0) && noeud1.at(1) == noeud2.at(1) ){
    return true;
  }
  
  return false;
  
}

// Get position
vector<int> Arbre::getPos(){
  return this->Pos;
}
// get list of position of the  path
vector< vector< int > > Arbre::getListPosChemin()
{
  return listPosChemin;
}

// Display the path 
void Arbre::affichageChemin()
{
  for(int i = 0 ; i < listPosChemin.size(); i++){
    cout << "noeud = (" << (listPosChemin.at(i)).at(0) << ", " << (listPosChemin.at(i)).at(1) << ")" << endl;
  }
}

// Set a node child in the tree from the position and node indicated in arg
void Arbre::setFilsPosition(Arbre& position, vector<int> noeud, vector<int> fils){//position = a partir de tel noeur ; noeud = la ou on veut l'ajouter ; fils celui qu'on veut ajouter
 Arbre temp = position;
 //Arbre New(fils);
 if( CompareNoeud(position.Pos, noeud) == true){
   //cout << "ajout du noeud" << endl;
   Arbre New(fils);
   position.fils.push_back(New);
 }
 else{
  if(position.fils.size() != 0){
    for(int i = 0; i < position.fils.size(); i++){ 
	position.setFilsPosition(position.fils.at(i), noeud, fils);
    }
  }
 }
  
}

// Set a node child in the tree from the node indicated in arg
void Arbre::setFils(vector<int> noeud, vector<int> fils){//noeud = la ou on veut l'ajouter ; fils celui qu'on veut ajouter
 
  setFilsPosition(*this ,noeud, fils);
}

// display all node starting the position node
void Arbre::affichage(Arbre position){
    affichageNoeud(position);
    if(position.fils.size() != 0){
	for(int i = 0; i < position.fils.size(); i++){
	    //cout <<  "branche numero " << i << " " << endl;
	    position.affichage(position.fils.at(i));
	}
    }
}

// Display a node
void Arbre::affichageNoeud(Arbre position){
  cout << "noeud = (" << position.Pos.at(0) << ", " << position.Pos.at(1) << ")" << endl;
}

// Compute the tree branch whose the final child is qf. The result is in ListPosChemin.
void Arbre::List(Arbre position, vector<int> qf){
  conditionArret = false;
  ComputeList(position,qf);
}

// Compute the tree branch whose the final child is qf. The result is in ListPosChemin
void Arbre::ComputeList(Arbre position, vector<int> qf){
  //cout << " Entre fonction : " << qf.at(0) << " " << qf.at(1) << " " << position.Pos.at(0) << " " << position.Pos.at(1) << endl;

  if( CompareNoeud(position.Pos, qf) != true &&  position.fils.size() != 0){
      int i = 0; 
      vector<int> temp = position.getPos();
      listPosChemin.push_back(temp);
      //cout << " Ajouter : " << temp.at(0) << " " << temp.at(1) << endl;
      while(i < position.fils.size() && conditionArret == false){
	
	
	List(position.fils.at(i) , qf);
	i++;
	//cout << i << endl;
      }
      
      if(conditionArret == false){
	listPosChemin.erase(find(listPosChemin.begin(),listPosChemin.end(), temp)); 
	//cout << " erased : " << temp.at(0) << " " << temp.at(1) << endl;
      }
 }
 else{
    if(CompareNoeud(position.Pos, qf) == true){
      conditionArret = true;
      vector<int> temp = position.getPos();
      listPosChemin.push_back(temp);
      //cout << " Ajouter : " << temp.at(0) << " " << temp.at(1) << endl;
    }
  }
}