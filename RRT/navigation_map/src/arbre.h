#ifndef ARBRE_H
#define ARBRE_H
#include <iostream>
#include <list>
#include <vector>

class Arbre
{
private:
    std::vector<int> Pos;
    std::vector<Arbre> fils;
    std::vector<std::vector<int> > listPosChemin;
    void ComputeList(Arbre position, std::vector<int> qf);
    void setFilsPosition(Arbre& position,std::vector<int> noeud, std::vector<int> fils);
    void affichageNoeud(Arbre position);
public:
    Arbre();
    
    Arbre(std::vector<int> racine);
    void setFils(std::vector<int> noeud, std::vector<int> fils);
    bool CompareNoeud(std::vector<int> noeud1, std::vector<int> noeud2);
    void affichage(Arbre position);

    void List(Arbre position, std::vector<int> qf);
    std::vector<int> getPos();
    std::vector< std::vector<int> > getListPosChemin();
    void affichageChemin();
};

#endif

