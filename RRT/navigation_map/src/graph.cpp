#include "graph.h"
#include <boost/concept_check.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <ctime>

using namespace std;


/* Function */
long Factoriel(long n) {
	return n > 1?(n * Factoriel(n-1)):1;
}

long Combinaison(long n, long k){
	return Factoriel(n)/(Factoriel(k) * Factoriel(n-k)); 
}

// Convert the path to the type nav_msgs::Path in the map referential
nav_msgs::Path Graph::ConvertChemin(vector< std::vector<int> > chemin, int origineX, int origineY){
	nav_msgs::Path chemin_convert;
	chemin_convert.header.frame_id = "/map";
	
	int taille =chemin.size();
	//cout << "inside the convert's function " << endl;
	std::vector<geometry_msgs::PoseStamped> liste_position;
	for(int i = 0; i< taille; i++){
		geometry_msgs::PoseStamped position;
		position.pose.position.x = (chemin[i].at(0) *resolution + origineX);
		position.pose.position.y = (tailleY -chemin[i].at(1))*resolution + origineY;
		/*position.pose.position.x = (chemin[i].at(0)  + origineX)*resolution;
		 *    position.pose.position.y = (tailleY -chemin[i].at(1) + origineY)*resolution;*/
		position.pose.position.z = 0;
		position.pose.orientation.w =1;
		position.pose.orientation.x = 0;
		position.pose.orientation.y = 0;
		position.pose.orientation.z = 0;
		liste_position.push_back(position);
	}
	liste_position = add_point(liste_position);
	
	for(int j=0; j< liste_position.size() ; j++){
		chemin_convert.poses.push_back(liste_position[j]);
		//cout << j << " x = " << liste_position[j].pose.position.x << " y = " << liste_position[j].pose.position.y << endl;
	}
	
	
	
	
	return chemin_convert;
}

// compute the distance between two points p1 and p2
float dist_pose(geometry_msgs::PoseStamped p1 , geometry_msgs::PoseStamped p2){
	return sqrt(pow((p1.pose.position.x - p2.pose.position.x),2) + pow((p1.pose.position.y - p2.pose.position.y),2));
	
}

// Add point in a vector of geometry_msgs::PoseStamped
std::vector< geometry_msgs::PoseStamped > add_point(std::vector< geometry_msgs::PoseStamped > p){
	bool far_away = true;
	std::vector<geometry_msgs::PoseStamped>::iterator it;
	while(far_away){
		far_away = false;
		for(int i = 0 ; i < p.size() - 1 ; i++){
			it = p.begin() + i;
			float distance = dist_pose(p[i],p[i+1]);
			if(distance > 0.001){
				far_away = true;
				geometry_msgs::PoseStamped interm;
				interm.header.frame_id = p[i].header.frame_id;
				interm.header.stamp = ros::Time::now();
				interm.pose.position.x = (p[i].pose.position.x + p[i+1].pose.position.x) / 2;
				interm.pose.position.y = (p[i].pose.position.y + p[i+1].pose.position.y) / 2;
				p.insert(it + 1,interm);
			}
		}
	}
	std::cout << p.size() << std::endl;
	return p;
}



/****************************/
/* Method of the Graph class*/
/****************************/



bool aff = false;

// Constructor 
Graph::Graph(){
	BoolMap = false;
	srand(time(0));
}



// Destructor
Graph::~Graph(){
	//eraseAllPoint();
}
/*void Graph::eraseAllPoint(){
	point_x.clear();
	point_y.clear();
}*/

/***********/
/* Getters */
/***********/

vector< vector<int> > Graph::getChemin(){
	return G; 
}

vector< vector<int> > Graph::getCheminLisse(){
	return Glisse; 

}

int Graph::getX_origin()
{
	return x_origin;
}

int Graph::getY_origin()
{
	return y_origin;
}

double Graph::getResolution()
{
	return resolution;
}

int Graph::getTailleX()
{
	return tailleX;
}

int Graph::getTailleY()
{
	return tailleY;
}

/*vector<int> Graph::getPoint_x(){
	return this->point_x;
}

vector<int> Graph::getPoint_y(){
	return this->point_y;
	
}*/

/***********/
/* Setters */
/***********/

void Graph::setPoint_x(int ValueToAdd){
	point_x.push_back(ValueToAdd);
}

void Graph::setPoint_y(int ValueToAdd){
	point_y.push_back(ValueToAdd);
}

void Graph::setPoint(int x, int y){
	setPoint_x(x);
	setPoint_y(y);
}





/************************************/
/*               Map                */
/************************************/

// Launch a subscriber to receive the map in the topic /map
void Graph::loadMap(){
	listener = n.subscribe("/map",100, &Graph::mapCallback,this);
	ros::Rate r(10);
	//cout << "avant while " << BoolMap << endl;
	while(n.ok() && BoolMap == false){
		// cout << " dans la boucle" << endl;
		ros::spinOnce();
		//r.sleep();
	}
	listener.shutdown();
	
}

// Create the OpenCV mat image from the map whose type is OccupancyGrid
bool Graph::CreateImage(){//Creer l'image a partir de l'OccupancyGrid
	//if(&map != NULL){
	if(&map !=NULL){
		cv::Mat Temp(tailleY, tailleX, CV_8UC3, cv::Scalar(255, 255, 255) );
		
		for(int xi=0; xi < tailleX; xi++){
			for(int yi=0;yi< tailleY; yi++){
				if( ValueNavMap(xi, yi ) == -1){
					cv::Vec3b couleur = Temp.at<cv::Vec3b>(yi,xi);
					couleur[0] = 206; couleur[1] = 206; couleur[2] = 206;
					Temp.at<cv::Vec3b>(yi,xi) = couleur;
				}
				else if(ValueNavMap(xi, yi ) > 0){
					cv::Vec3b couleur = Temp.at<cv::Vec3b>(yi,xi);
					couleur[0] = 0; couleur[1] = 0; couleur[2] = 0;
					Temp.at<cv::Vec3b>(yi,xi) = couleur;
				}  
			}
		}
		Temp.copyTo(Carte);
		return true;  
	}
	return false;
}


// Give the value in map depending of x and y
int Graph::ValueNavMap(int x, int y ){
	return map.data[(tailleX)*(tailleY - 1 - y)+ x ];
}

// Change the value of the pixel in map  depending of x and y
void Graph::ChangeValueNavMap(int x, int y, int value ){
	map.data.at(tailleX*(tailleY - 1 - y)+ x) = value;
}

// Callback to receive the map in the topic /map and save it in the variable map.
void Graph::mapCallback(nav_msgs::OccupancyGrid msg){
	//cout << "dans le callback" << endl;
	map = msg;
	BoolMap = true;
	tailleX = map.info.width;
	tailleY = map.info.height;
	x_origin = round(map.info.origin.position.x);
	y_origin =round(map.info.origin.position.y);
	resolution = map.info.resolution;
	cout << "Save map realized ! " << tailleX << " x " << tailleY  << " origine = ["<< x_origin << ", " << y_origin << "] "<< endl;
	//printf("%d %d \n",map->data[1700*4000+2000],map->data[2000*4000+1700]);
	
}

// Apply a filter on the map whose variable is Carte so as to reduce the number of pixel grey and raise the thickness of the obstacles
void Graph::Filtrage(){
	cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,cv::Size(10,10),cv::Point( 2, 2 ) );
	cv::dilate(Carte,Carte, element);
	
	for(int xi=0; xi < tailleX; xi++){
		for(int yi=0;yi< tailleY; yi++){
			
			
			cv::Vec3b couleur = Carte.at<cv::Vec3b>(yi,xi);
			if(ValueNavMap(xi, yi ) > 0){
				couleur[0] = 0;
				couleur[1] = 0;
				couleur[2] = 0;
				Carte.at<cv::Vec3b>(yi,xi) = couleur;
			}
			
			if(couleur[0]==0 && couleur[1] == 0 && couleur[2] == 0){
				ChangeValueNavMap(xi, yi,100);
			}
			else if(couleur[0] == 206 && couleur[1] == 206 && couleur[2] == 206){
				ChangeValueNavMap(xi, yi, -1);
			}
			else if(couleur[0] == 255 && couleur[1] == 255 && couleur[2] == 255){
				ChangeValueNavMap(xi, yi, 0);
			}
			else{
				cout <<" erreur lors de la dilation" << endl; 
			}
			
			
		}
	}
	cv::erode(Carte,Carte, element);
	for(int xi=0; xi < tailleX; xi++){
		for(int yi=0;yi< tailleY; yi++){
			
			
			cv::Vec3b couleur = Carte.at<cv::Vec3b>(yi,xi);
			
			if(couleur[0]==0 && couleur[1] == 0 && couleur[2] == 0){
				ChangeValueNavMap(xi, yi,100);
			}
			else if(couleur[0] == 206 && couleur[1] == 206 && couleur[2] == 206){
				ChangeValueNavMap(xi, yi, -1);
			}
			else if(couleur[0] == 255 && couleur[1] == 255 && couleur[2] == 255){
				ChangeValueNavMap(xi, yi, 0);
			}
			else{
				cout <<" erreur lors de la dilation" << endl; 
			}
			
			
		}
	}
	
}



/***********/
/* Display */
/***********/


// Display the map
void Graph::displayImage(){
	cvNamedWindow("Windows",0);
	cv::Mat image2 = Carte.clone();
	cv::imshow("Windows",image2);
	cv::waitKey(0);
}

// Display the path computed by RRT algorithm
void Graph::DisplayChemin(vector<vector<int> > graph, string name){ // Graph has the coordinates x and y of the path, name is the name of the windows (so as to display the map)
	cvNamedWindow(name.c_str(),0);
	cv::Mat img = Carte.clone();
	cv::circle(img,cv::Point(graph[0].at(0), graph[0].at(1)),5,cv::Scalar(255,0,0),-1,8); 
	for(int i = 1; i < graph.size(); i++){
		cv::circle(img,cv::Point(graph[i].at(0), graph[i].at(1)),5,cv::Scalar(255,0,0),-1,8); 
		cv::line(img,cv::Point(graph[i-1].at(0), graph[i-1].at(1)), cv::Point(graph[i].at(0), graph[i].at(1)), cv::Scalar(255,0,0), 1, 8, 0);
		cv::waitKey(1);
	}
	cv::imshow(name.c_str(),img);
	cv::waitKey(0);// presskey to close the windows
}

// print the value on the terminal of the vector v
void Graph::Affichage(vector< vector<int> > v){
	for(int s = 0; s < v.size(); s++){
		cout << v[s].at(0) << " " << v[s].at(1) << endl; 
	}
	
}

//Function which draw qpoint on the map. We can choose the color and diameter of the circle.
void Graph::DrawOnlyCircle(cv::Mat img, vector<int> qpoint, string nameWindow, int wait,cv::Scalar color, int diametre){
	cvNamedWindow(nameWindow.c_str(),0);
	cv::circle(img, cv::Point(qpoint.at(0),qpoint.at(1)), diametre,color,-1,8);
	
	cv::imshow(nameWindow.c_str(),img);
	cv::waitKey(wait);
}
//Function which draw qpoint on the map. We can choose the color and diameter of the circle. A line is draw between qpoint and qnear
void Graph::DrawCircleLine(cv::Mat img, vector<int> qpoint, vector<int> qnear, string nameWindow, int wait,cv::Scalar color1,  int diametre){
	cvNamedWindow(nameWindow.c_str(),0);
	cv::circle(img, cv::Point(qpoint.at(0),qpoint.at(1)), diametre,color1,-1,8);
	cv::line(img,cv::Point(qnear.at(0), qnear.at(1)), cv::Point(qpoint.at(0), qpoint.at(1)),  color1, 1, 8, 0);
	cv::imshow(nameWindow.c_str(),img);
	cv::waitKey(wait);
}

void Graph::test()
{
	cv::Mat img = Carte.clone();//cv::imread(fichier,CV_LOAD_IMAGE_COLOR);
	cv::Mat image = Carte.clone();//cv::imread(fichier,CV_LOAD_IMAGE_COLOR);
	bool obstacle = false;
	double qnear[] = {1138, 533};
	double qf[] = {1549, 1151};
	double qrand[] = {1645, 541};
	double a =0.0 ,b=0.0;
	int vect[2];//vecteur directeur
	vect[1] = qrand[0] - qnear[0];//y
	vect[0] = qrand[1] - qnear[1];//x
	double angle = atan2(vect[1],vect[0]);//angle par rapport au repere map
	int deltaq = 100;
	double x1 = 1.0, y1 = 1.0;
	double x = 0.0, y = 0.0;
	double distance = distance = getDistance(qnear[0], qnear[1], qrand[0], qrand[1]);
	// Verification du sens
	if(vect[0]<0){
		x1 = -1.0;
	}
	
	if(vect[1]<0){
		y1 = -1.0;
	}
	
	
	if(deltaq >= distance){
		deltaq = distance;    
	}
	
	// On verifie qu'il n'y a pas d'obstacle sur le chemin_fini
	if(vect[0] == 0){// si la droite n'est que selon y, x et y repere cartesien , pas opencv!!
		x = qnear[1];
		for(y = qnear[0]; y!= round(qnear[0]+copysign(deltaq,vect[1])) ; y = y + y1){
			//if( map->data[(tailleX)*(tailleY - 1 - x)+y ] != 0 ){
			if(ValueNavMap(x,y) != 0 ){
				obstacle = true;
				
			}
		}
	}else{//si la droite depend de x
		cout <<"boucle"<<endl;
		a = vect[1]*1.0/vect[0];
		b = -a*qnear[1] + qnear[0];
		//cv::line(image,cv::Point( qear[0], a*qnear[1]+b), cv::Point(qrand(.at(0),new1.at(1)),  cv::Scalar(255,0,0), 1, 8, 0);
		if(aff==true)
			cout << "no error for now " << endl;
		x = qnear[1]*1.0;
		while( round(x)!=round(qnear[1]+cos(angle)*deltaq)){
			y =1.0*( a*x + b);
			//if(aff==true)
			cout << " x y a b = " << x << " " << y << " " << a << " " << b << endl;
			cv::Vec3b couleur = img.at<cv::Vec3b>(y,x);
			couleur[0] = 255;
			couleur[1] = 0;
			couleur[2] = 0;
			img.at<cv::Vec3b>( cv::Point(y,x) ) =  couleur;
			//cout << " x y a b = " << y << " " << x <<  endl;
			//if( map->data[(tailleX)*(tailleY - 1 - x)+y ] != 0 ){// blanc
			if(ValueNavMap(x,y)==0){
				obstacle = true;
				cout << " obstacle " << endl;
			}
			x = x+(x1/20);
			//cout << y << endl;
			//}
		}
		if(aff==true)
			cout << "no error for now " << endl;
	}
	
	//dernier increment
	//if(obstacle == false && map->data[(tailleX)*(tailleY - 1 - y)+x ] != 0 ){
	if(obstacle == false && ValueNavMap(x,y) != 0 ){
		obstacle = true;
	}
	
	//if(obstacle == false ){
	//qnewPoint.push_back(y);//x dans le repere opencv
	//qnewPoint.push_back(x);//y dans le repere opencv
	//qnewPoint.push_back(qnear[0]+sin(angle)*deltaq);//x dans le repere opencv
	//qnewPoint.push_back(qnear[1]+cos(angle)*deltaq);//y dans le repere opencv
	/*cout << "angle : " << angle*360/(2*3.141) << endl;
	 *      cout << "qrand : " << qf[0] << " " << qf[1] << "qnear : " << qnear[0] << " " << qnear[1] << endl;
	 *      cout << "vect " << vect[0] << " " << vect[1] << endl;
	 *      cout << " x & y " << y << " " << x << " " << qnear[0]+sin(angle)*deltaq << " " << qnear[1]+cos(angle)*deltaq  << " " << endl;*/
	
	
	//}
	cvNamedWindow("Windows",0);
	cv::circle(image,cv::Point(qnear[0],qnear[1]),5,cv::Scalar(255,0,0),-1,8);
	cv::circle(image,cv::Point(qnear[0]+sin(angle)*deltaq,qnear[1]+cos(angle)*deltaq),5,cv::Scalar(255,0,0),-1,8);
	cv::circle(image,cv::Point(y,x),5,cv::Scalar(255,0,0),-1,8);
	cv::imshow("Windows",img); 
	cv::waitKey(0);
}

/*******************************/
/* Methode pour la méthode RRT */
/*******************************/

// Grab a random point possible inside the map
void Graph::RRT_qrand(vector<int>& tab , int t_X, int t_Y){
	bool obstacle = true;
	while(obstacle == true){
		if(tab.size() != 0 ){
			tab.clear();
		}
		
		tab.push_back( rand()%t_X );  // ( 0 - 4);
		tab.push_back( rand()%t_Y ); // ( 0 - 4)
		
		if( map.data[ (tailleX) * ( t_Y-1-tab.at(1) ) + tab.at(0) ] == 0){
			obstacle = false;
		}
		
		
	}
}

// Compute the distance between two point
int Graph::getDistance(int x1, int y1, int x2, int y2){
	return sqrt(pow( x2 - x1, 2) + pow( y2 - y1 , 2));
}

// Compute the qnear point which is the nearest point in the Graph from qrand. In the vector qrand et qnear you have two coordinates, x and y.
void Graph::RRT_qnear(vector<int>& qnear, int& distance, vector<int> qrand, vector< vector<int> > Graph){
	/* Initialisation */
	qnear.push_back(Graph[0][0]);
	qnear.push_back(Graph[0][1]);
	distance = getDistance(qnear.at(0), qnear.at(1), qrand.at(0), qrand.at(1));
	
	for(int i = 1; i < Graph.size(); i++){ // On cherche le point le plus proche du qrand
		int d_temp = getDistance(Graph[i][0], Graph[i][1], qrand.at(0), qrand.at(1) );
		if(d_temp < distance){ 
			qnear.clear();
			qnear.push_back(Graph[i][0]);
			qnear.push_back(Graph[i][1]);
			distance = d_temp;
		}
	}
	//cout << "qnear = (" << qnear[0] << "," << qnear[1] << ")" << endl;
	
}

// Return true if the point is inside an obstacle
bool Graph::IsObstacleSansPortee(int x, int y){
	if( /*map->data[(tailleX)*(tailleY - 1 - y)+ x ]*/ ValueNavMap(x, y ) != 0 ){
		return true;
	}
	return false;
}

// Return true if the point is inside an obstacle or near with a distance "pixel" of an obstacle
bool Graph::IsObstacleAvecPortee(int x, int y, int pixel){
	int xi = x - pixel, xf = x + pixel, yi = y - pixel, yf = y + pixel;
	if( xi < 0 )
		xi = 0;
	if(xf >= tailleX)
		xf = tailleX-1;
	if( yi < 0 )
		yi = 0;
	if(yf >= tailleY)
		yf = tailleY-1;
	for(int i = xi; i <= xf ; i++){
		for(int j = yi; j <= yf ; j++){
			if( /*map->data[(tailleX)*(tailleY - 1 - j)+ i ]*/ ValueNavMap( i, j ) != 0 ){
				return true;
			}
		}
	}
	return false;
}

// Function which extend the graph depending on qrand and qnear. deltaq is the distance between qnear et qnewpoint (if no obstacle). if qnewpoint is near to a value pixel of an obstacle the graph cannot be extend.
bool Graph::RRT_extend(vector< vector<int> > Graph, vector<int>& qrand, int deltaq, vector<int>& qnewPoint, vector<int>& qnear, int pixel){
	//int Qnear[2]; 
	//cout << "qrand = (" << qrand.at(0) << "," << qrand.at(1) << ")" << endl;
	int distance;
	double angle, a , b;//angle du vecteur directeur par rapport au repere
	int vect[2];//vecteur directeur
	
	// On cherche le point le plus proche de qrand
	
	RRT_qnear(qnear, distance, qrand, Graph); 
	
	//Valeur du vecteur directeur entre qinit et qrand, on suppose que x est vers le bas , y vers al droite
	
	vect[1] = qrand.at(0) - qnear.at(0);//y
	vect[0] = qrand.at(1) - qnear.at(1);//x
	angle = atan2(vect[1],vect[0]);//angle par rapport au repere map
	
	
	
	if(aff==true)
		cout << "Vec = ("<< vect[0] << "," << vect[1] << ")    angle :" << angle << "  distance = " << distance << endl;
	
	//Verification si pas d'obstacle
	bool obstacle = false;
	double x1 = 1.0, y1 = 1.0, x = 0.0, y = 0.0;
	// Verification du sens
	if(vect[0]<0){
		x1 = -1.0;
	}
	
	if(vect[1]<0){
		y1 = -1.0;
	}
	
	
	if(deltaq >= distance){
		deltaq = distance;    
	}
	
	// On verifie qu'il n'y a pas d'obstacle sur le chemin_fini
	if(vect[0] == 0){// si la droite n'est que selon y, x et y repere cartesien , pas opencv!!
		x = qnear.at(1);
		for(y = qnear.at(0); round(y)!= round(qnear.at(0)+copysign(deltaq,vect[1])) ; y = y + y1){
			/*if( map->data[(tailleX)*(tailleY - 1 - (int)x)+(int)y ] != 0 ){
			 *		  obstacle = true;
			 *		  break;
		}*/
			obstacle = IsObstacleAvecPortee((int)y, (int)x, pixel );
			if(obstacle == true)
				break;
		}
	}
	
	else if(abs(vect[1]) > abs(vect[0])){ // si le vecteur y est plus grand que le x (dans le repere cartesien)
		
		a = vect[1]*1.0/vect[0];
		b = -a*qnear.at(1) + qnear.at(0);
		//cv::line(image,cv::Point( qear[0], a*qnear[1]+b), cv::Point(qrand(.at(0),new1.at(1)),  cv::Scalar(255,0,0), 1, 8, 0);
		
		y = qnear.at(0);
		// cout << " vect a b = " << vect[0] << " " << vect[1] << " " << a << " " << b << endl;
		while(round(y)!=round(qnear.at(0)+sin(angle)*deltaq)){
			x = (y-b)/a ;
			obstacle = IsObstacleAvecPortee((int)y, (int)x, pixel);
			if(obstacle == true)
				break;
			
			y = y+y1;
			
			
		}
	}
	else{//si la droite depend de x
		a = vect[1]*1.0/vect[0];
		b = -a*qnear.at(1) + qnear.at(0);
		//cv::line(image,cv::Point( qear[0], a*qnear[1]+b), cv::Point(qrand(.at(0),new1.at(1)),  cv::Scalar(255,0,0), 1, 8, 0);
		
		x = qnear.at(1);
		//cout << " vect a b = " << vect[0] << " " << vect[1] << " " << a << " " << b << endl;
		while(round(x)!=round(qnear.at(1)+cos(angle)*deltaq)){
			y = a*x + b;
			obstacle = IsObstacleAvecPortee((int)y, (int)x, pixel);
			if(obstacle == true)
				break;
			
			x = x+x1;
			
			
		}
		
	}
	
	
	
	//dernier increment
	if(obstacle == false  ){
		obstacle = IsObstacleAvecPortee((int)y, (int)x, pixel);
	}
	
	if(obstacle == false ){
		qnewPoint.push_back((int)y);//x dans le repere opencv
		qnewPoint.push_back((int)x);//y dans le repere opencv
		//qnewPoint.push_back(qnear[0]+sin(angle)*deltaq);//x dans le repere opencv
		//qnewPoint.push_back(qnear[1]+cos(angle)*deltaq);//y dans le repere opencv
		/*cout << "angle : " << angle*360/(2*3.141) << endl;
		 *      cout << "qrand : " << qrand.at(0) << " " << qrand.at(1) << "qnear : " << qnear.at(0) << " " << qnear.at(1) << endl;
		 *      cout << "vect " << vect[0] << " " << vect[1] << endl;
		 *      cout << " x & y " << y << " " << x << " " << qnear.at(0)+sin(angle)*deltaq << " " << qnear.at(1)+cos(angle)*deltaq << endl;*/
		return true;
		
	}
	
	return false;
}

// Check if there is no obstacle between q1 and q2 . If not, it means that RRT algorithm is finished.
bool Graph::RTT_IsArrived(vector<int> q1, vector<int> q2, int deltaq, int pixel){
	int distance = getDistance(q1.at(0), q1.at(1), q2.at(0), q2.at(1));
	cout << "IS ARRIVED distance = "<< distance << endl; 
	int vect[2];
	bool obstacle = false;
	double a=0, b=0;
	if(distance == 0)
		return true;
	if(distance <= 3*deltaq){
		double x1 = 1.0, y1 = 1.0, x = 0.0, y = 0.0;
		// y a droite x vers le bas
		/*vect[1] = q2.at(0) - q1.at(0);//y
		vect[0] = q2.at(1) - q1.at(1);//x
		
		
		// Verification du sens : sign
		if(vect[0]<0){
			x1 = -1.0;
		}
		
		if(vect[1]<0){
			y1 = -1.0;
		}
		// On verifie qu'il n'y a pas d'obstacle sur le chemin_fini
		if(vect[0] == 0){// si la droite n'est que selon y
			x = q1.at(1);
			for(y = q1.at(0); round(y)!= round(q1.at(0)+copysign(deltaq,vect[1])) ; y = y + y1){
				obstacle = IsObstacleAvecPortee((int)y, (int)x, pixel);
				if(obstacle == true)
					break;
			}
		}else if(abs(vect[1]) > abs(vect[0])){ // si le vecteur y est plus grand que le x (dans le repere cartesien)
			
			a = vect[1]*1.0/vect[0];
			b = -a*q1.at(1) + q1.at(0);
			//cv::line(image,cv::Point( qear[0], a*qnear[1]+b), cv::Point(qrand(.at(0),new1.at(1)),  cv::Scalar(255,0,0), 1, 8, 0);
			
			y = q1.at(0);
			//cout << " vect a b = " << vect[0] << " " << vect[1] << " " << a << " " << b << endl;
			while(round(y)!=q2.at(0)){
				x = (y-b)/a ;
				obstacle = IsObstacleAvecPortee((int)y, (int)x, pixel);
				if(obstacle == true)
					break;
				
				y = y+y1;
				
				
			}
		}    
		
		else{//si la droite depend de x
			a = vect[1]*1.0/vect[0];
			b = -a*q1.at(1) + q1.at(0);
			if(aff==true)
				cout << "no error for now " << endl;
			for(x = q1.at(1); x != q2.at(1) ; x = x+x1){
				y = a*x + b;
				if(aff==true)
					cout << " x y a b = " << x << " " << y << " " << a << " " << b << endl;
				obstacle = IsObstacleAvecPortee((int)y, (int)x, pixel);
				if(obstacle == true)
					break;
				//}
			}
			if(aff==true)
				cout << "no error for now " << endl;
		}*/
		
		vect[0] = q2.at(0) - q1.at(0);
		vect[1] = q2.at(1) - q1.at(1);
		if(vect[0]<0){
			x1 = -1.0;
		}
		
		if(vect[1]<0){
			y1 = -1.0;
		}
		int xi,yi;
		if(vect[0] == 0){ // selon y
			yi = q1.at(1);
			do{
				obstacle = IsObstacleAvecPortee((int)xi, (int)yi, pixel);
				if(obstacle == true)
					break;
				yi = yi + y1;
			}while(yi != q2.at(1));
			
		}
		else if(vect[1] == 0){ // selon x
			xi = q1.at(0);
			do{
				obstacle = IsObstacleAvecPortee((int)xi, (int)yi, pixel);
				if(obstacle == true)
					break;
				xi = xi + x1;
			}while(xi != q2.at(0));
			
		}
		else if(abs(vect[0]) > abs(vect[1]) ){
			a = vect[1]*1.0/vect[0];
			b = -a*q1.at(0) + q1.at(1);
			xi = q1.at(0);
			yi = a*xi + b;
			do{
				obstacle = IsObstacleAvecPortee((int)xi, (int)yi, pixel);
				if(obstacle == true)
					break;
				xi = xi + x1;
				yi = a*xi + b;
			}while(xi != q2.at(0));
		}
		else{
			a = vect[1]*1.0/vect[0];
			b = -a*q1.at(0) + q1.at(1);
			yi = q1.at(1);
			
			do{
				obstacle = IsObstacleAvecPortee((int)xi, (int)yi, pixel);
				if(obstacle == true)
					break;
				yi = yi + y1;
				xi = (yi-b)/a;
			}while(yi != q2.at(1));
			
		}
		return !obstacle;
	}
	
	return false;
}

// RRT Algorithm, the algorithm stop if the number of increment is exceeded or if a path is found. Return true if a path is found.
bool Graph::RRT_Connect(vector<int> qinit, vector<int> qfinal , int deltaq, int increment, int pixel ){
	bool draw = true;
	
	// Check if the initial position is possible (no obstacle)
	if(IsObstacleSansPortee(qinit.at(0),qinit.at(1)) == true ){
		cout << "position de départ impossible " << endl;
		return false;
	}
	
	// Check if the final position is possible (no obstacle)
	if(IsObstacleSansPortee(qfinal.at(0), qfinal.at(1))==true){
		cout << "position final impossible " << endl;
		return false;
	}
	
	bool chemin_fini = false;
	//cv::Mat image = cv::imread(fichier,CV_LOAD_IMAGE_COLOR);
	cv::Mat image = Carte.clone();
	bool couleur = true; // true = red  => permit to know which graph represent the initial position and final position ( Gi and Gf) because we will swap them in the RRT algorthm
	vector<int> qrand;
	vector<int> qrand2;
	vector< vector<int> > Gf;
	vector< vector<int> > Gi;
	Gi.push_back(qinit);
	Gf.push_back(qfinal);
	Arbre Ginit(qinit); // couleur true
	Arbre Gfinal(qfinal);//couleur false
	vector<int> new1;
	vector<int> new2;
	vector<int> qnear1, qnear2;
	int i = 0;
	int wait = 1;
	
	if(draw ==true){
		DrawOnlyCircle(image, qinit, "Windows", 1,cv::Scalar(0,0,0), 5);
		DrawOnlyCircle(image, qfinal, "Windows", 1,cv::Scalar(0,0,0), 5);
	}
	
	// While the increment doesn't surpass the condition or the path is not found yet
	while(chemin_fini == false && i < increment){
		
		// Compute a qrand
		RRT_qrand(qrand, tailleX, tailleY);
		
		//cv::circle(image,cv::Point(qrand.at(0),qrand.at(1)),10,cv::Scalar(0,255,0),-1,8);
		//cout << "TAILLE " << tailleX << " " << tailleY << endl;
		//cout << qrand.at(0) << " " << qrand.at(1) << endl;
		
		// Condition to only extend the graph
		if(!(i%10 <10 && i%10 <= 7) ){
			//extend the graph if it is possible
			if(RRT_extend(Gi, qrand, deltaq, new1, qnear1, pixel) == true){
				Gi.push_back(new1);
				if(couleur == true){
					//cout << "GINIT" << endl;
					Ginit.setFils(qnear1, new1);
					
					if(draw ==true){
						DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(255,0,0),  5);
					}
				}else{
					//cout <<" GFINAL" << endl;
					Gfinal.setFils(qnear1, new1);
					//cout << "POS : " << Gfinal.fils.at(0).Pos.at(0) << " " << Gfinal.fils.at(0).Pos.at(1) << endl;
					if(draw ==true){
						DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(0,0,255),  5);
					}
				}
				Gi.swap(Gf);
				couleur = !couleur;
				i++;
			}
			new1.clear();
			new2.clear();
		}
		else{ // Condition to extend the graph and then check if a path is found.
			
			if(RRT_extend(Gi, qrand, deltaq, new1, qnear1, pixel) == true){//extension à partir du graphe de qi = extend the graph from qi
				i++;
				if(RRT_extend(Gf, new1, deltaq, new2, qnear2, pixel) == true){ // extension à partir du graphe de qf = extend the graph from qf
					Gi.push_back(new1);
					Gf.push_back(new2);
					
					// Affichage point / droite = draw point/ line
					if(couleur == true){
						Ginit.setFils(qnear1, new1);
						Gfinal.setFils( qnear2, new2);
						if(draw ==true){
							DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(255,0,0),  5);
							DrawCircleLine(image, new2, qnear2, "Windows", wait,cv::Scalar(0,0,255),  5);
						}
						if(aff==true)
							cout << "bleu" << endl;
					}
					else{
						Gfinal.setFils(qnear1, new1);
						Ginit.setFils(qnear2, new2);
						
						if(draw ==true){
							DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(0,0,255),  5);
							DrawCircleLine(image, new2, qnear2, "Windows", wait,cv::Scalar(255,0,0),  5);
						}
						if(aff==true)
							cout << "red" << endl;
					}
					if(aff==true)
						cout<<" nouveau point : " << new1.at(0)<< " " << new1.at(1)<< "   " << new2.at(0)<< " " << new2.at(1)<< endl;
					
					
					// check if the two new point is near enough to complete a path
					if(RTT_IsArrived(new1, new2, deltaq, pixel) == true ){
						if(couleur==true){
							//Ginit.setFils(new1, new2);
							Ginit.List(Ginit,new1);
							Gfinal.List(Gfinal,new2);
							//cout << "true" << endl;
						}
						else{
							//Gfinal.setFils(new1, new2);
							Ginit.List(Ginit,new2);
							Gfinal.List(Gfinal,new1);
							
						}
					
						
						Gi.clear();
						Gi = Ginit.getListPosChemin();
						Gf.clear();
						Gf = Gfinal.getListPosChemin();
						//cout << "SIZE APRES: " << Gi.size() << " " << Gf.size() << endl;
						reverse(Gf.begin(), Gf.end());
						
						if(draw ==true){
							cv::line(image,cv::Point(new2.at(0),new2.at(1)), cv::Point(new1.at(0),new1.at(1)), cv::Scalar(0,0,255), 1, 8, 0);
						}
						cv::imshow("Windows",image);
						cv::waitKey(0);
						chemin_fini = true;
					}
					
				}
			}
			new1.clear();
			new2.clear();
		}
		qnear1.clear();
		qnear2.clear();
		
	}
	
	// If a path is found, save it in G graph
	if(chemin_fini == true){
		//cout << "Chemin trouvé " <<endl;
		G = Gi;
		G.insert(G.end(), Gf.begin(), Gf.end());
		return true;
	}
	else{
		cout << " chemin non trouvé " << endl; 
		return false;
	}
}

// RRT bidirectionnal Algorithm
bool Graph::RRT_Connectv2(vector<int> qinit, vector<int> qfinal , int deltaq, int increment, int pixel ){
	bool draw = true;
	// Check if the initial position is possible (no obstacle)
	if(IsObstacleSansPortee(qinit.at(0),qinit.at(1)) == true ){
		cout << "position de départ  " << endl;
		return false;
	}
	
	if(IsObstacleSansPortee(qfinal.at(0), qfinal.at(1))==true){
		cout << "position final impossible " << endl;
		return false;
	}
	// Check if the final position is possible (no obstacle)
	bool chemin_fini = false;
	//cv::Mat image = cv::imread(fichier,CV_LOAD_IMAGE_COLOR);
	cv::Mat image = Carte.clone();
	bool couleur = true; // true = red
	vector<int> qrand;
	vector<int> qrand2;
	vector< vector<int> > Gf;
	vector< vector<int> > Gi;
	Gi.push_back(qinit);
	Gf.push_back(qfinal);
	Arbre Ginit(qinit); // couleur true
	Arbre Gfinal(qfinal);//couleur false
	vector<int> new1;
	vector<int> new2;
	vector<int> qnear1, qnear2;
	int i = 0;
	int wait = 1;
	
	if(draw ==true){
		DrawOnlyCircle(image, qinit, "Windows", 1,cv::Scalar(0,0,0), 5);
		DrawOnlyCircle(image, qfinal, "Windows", 1,cv::Scalar(0,0,0), 5);
	}
	// While the increment doesn't surpass the condition or the path is not found yet
	while(chemin_fini == false && i < increment){
		
		RRT_qrand(qrand, tailleX, tailleY);
		
		if(RRT_extend(Gi, qrand, deltaq, new1, qnear1, pixel) == true){//extension à partir du graphe de qi
			Gi.push_back(new1);
			
			if(couleur == true){
				Ginit.setFils(qnear1, new1);
				//Gfinal.setFils( qnear2, new2);
				if(draw ==true){
					DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(255,0,0),  5);
					//DrawCircleLine(image, new2, qnear2, "Windows", wait,cv::Scalar(0,0,255),  5);
				}
				if(aff==true)
					cout << "bleu" << endl;
			}
			else{
				Gfinal.setFils(qnear1, new1);
				//Ginit.setFils(qnear2, new2);
				
				if(draw ==true){
					DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(0,0,255),  5);
					//DrawCircleLine(image, new2, qnear2, "Windows", wait,cv::Scalar(255,0,0),  5);
				}
				if(aff==true)
					cout << "red" << endl;
			}
			
			if(RRT_extend(Gf, new1, deltaq, new2, qnear2, pixel) == true){ // extension à partir du graphe de qf
				
				Gf.push_back(new2);
				
				// Affichage point / droite
				if(couleur == true){
					//Ginit.setFils(qnear1, new1);
					Gfinal.setFils( qnear2, new2);
					if(draw ==true){
						//DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(255,0,0),  5);
						DrawCircleLine(image, new2, qnear2, "Windows", wait,cv::Scalar(0,0,255),  5);
					}
					if(aff==true)
						cout << "bleu" << endl;
				}
				else{
					//Gfinal.setFils(qnear1, new1);
					Ginit.setFils(qnear2, new2);
					
					if(draw ==true){
						//DrawCircleLine(image, new1, qnear1, "Windows", wait,cv::Scalar(0,0,255),  5);
						DrawCircleLine(image, new2, qnear2, "Windows", wait,cv::Scalar(255,0,0),  5);
					}
					if(aff==true)
						cout << "red" << endl;
				}
				
				if(aff==true)
					cout<<" nouveau point : " << new1.at(0)<< " " << new1.at(1)<< "   " << new2.at(0)<< " " << new2.at(1)<< endl;
				
				
				cout << "p1 = " << new1.at(0) << " " << new1.at(1) << " p2 = " << new2.at(0) << " " << new2.at(1) << endl;
				if(RTT_IsArrived(new1, new2, deltaq, pixel) == true ){
					cout << "IS ARRIVED" << endl;
					if(couleur==true){
						//Ginit.setFils(new1, new2);
						Ginit.List(Ginit,new1);
						Gfinal.List(Gfinal,new2);
						//cout << "true" << endl;
					}
					else{
						//Gfinal.setFils(new1, new2);
						Ginit.List(Ginit,new2);
						Gfinal.List(Gfinal,new1);
						
					}
					//Gfinal.affichage(Gfinal);
					//cout<<" nouveau point : " << new1.at(0)<< " " << new1.at(1)<< "   " << new2.at(0)<< " " << new2.at(1)<< endl;
					//cout << "SIZE : " << Gi.size() << " " << Gf.size() << endl;
					Gi.clear();
					Gi = Ginit.getListPosChemin();
					Gf.clear();
					Gf = Gfinal.getListPosChemin();
					//cout << "SIZE APRES: " << Gi.size() << " " << Gf.size() << endl;
					reverse(Gf.begin(), Gf.end());
					
					if(draw ==true){
						cv::line(image,cv::Point(new2.at(0),new2.at(1)), cv::Point(new1.at(0),new1.at(1)), cv::Scalar(0,0,255), 1, 8, 0);
					}
					//cv::imshow("Windows",image);
					//cv::waitKey(0);
					chemin_fini = true;
				}
				
			}
			
		}
		
		if(chemin_fini == false){
			couleur = !couleur;
			Gi.swap(Gf);
		}
		qnear1.clear();
		qnear2.clear();
		new1.clear();
		new2.clear();
		i++;
		//cout << i << endl;
	}
	
	
	
	if(chemin_fini == true){
		//cout << "Chemin trouvé " <<endl;
		G = Gi;
		G.insert(G.end(), Gf.begin(), Gf.end());
		return true;
	}
	else{
		if(i>=increment){
			cout << "increment dépassé " << endl;
		}else{
			cout << " chemin non trouvé " << endl; 
		}
		return false;
	}
}

// Smoothing algorithm using Bezier Curves so as to smooth the path found by RRT algorithm
void Graph::GraphBezier(){
	vector<int> temp;
	vector<int> m1,m2;
	Glisse.push_back(G[0]);
	cout << G.size() <<endl;
	int i;
	for(i = 0 ; i < this->G.size()-2; i++){
		
		m1.push_back((int)round( (G[i].at(0)+G[i+1].at(0))/2 ) );
		m1.push_back((int)round( (G[i].at(1)+G[i+1].at(1))/2) );
		m2.push_back((int)round( (G[i+2].at(0)+G[i+1].at(0))/2) );
		m2.push_back((int)round( (G[i+2].at(1)+G[i+1].at(1))/2) );
	
		
		for(double t = 0.0; t <=1.0; t = t + 0.05){
			// double t = 0.5;
			temp.push_back( (int) (Combinaison(2,0) *(1-t)*(1-t)*m1.at(0) +  Combinaison(2,1) *t*(1-t)* G[i+1].at(0) + Combinaison(2,2) *t*t * m2.at(0))) ;
			temp.push_back( (int) (Combinaison(2,0) *(1-t)*(1-t)*m1.at(1) +  Combinaison(2,1) *t*(1-t)* G[i+1].at(1) + Combinaison(2,2) * t * t * m2.at(1))) ;
			//cout << " X : " << Combinaison(2,0) *(1-t)*(1-t)*m1.at(0) << " " << Combinaison(2,1) *t*(1-t)* G[i+1].at(0) << " " << Combinaison(2,2) * t * t * m2.at(0) << endl;
			//cout << " Y : " <<Combinaison(2,0) *(1-t)*(1-t)*m1.at(1) << " " << Combinaison(2,1) *t*(1-t)* G[i+1].at(1) << " " << Combinaison(2,2) * t * t * m2.at(1) << endl;
			//if(temp.at(0) > 2200 || temp.at(1) > 2200)
			//cout << "Erreur calcul " << endl;
			
			Glisse.push_back(temp);
			temp.clear();
		}
		
		
		m1.clear();
		m2.clear();
	}
	//cout << i << endl;
	Glisse.push_back(G[G.size() -1]);
	cout << " Bezier : " << "posinit = " << Glisse[0].at(0) << " " << Glisse[0].at(1) << " PosFinal = " << Glisse[Glisse.size()-1].at(0) << " " << Glisse[Glisse.size()-1].at(1) <<endl;

}
